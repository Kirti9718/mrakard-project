import React, { useEffect, useState,Image,ImageBackground } from 'react'
import { View } from 'react-native';
import AnimatedSplash from "react-native-animated-splash-screen";
import AppNavigation from "./src/Routes/AppNavigation"

export default App = () => {
  const [isLoaded, setLoader] = useState(false);
  useEffect(() => {
    setTimeout(() => setLoader(true), 5000)
  })

  return (<>

 
    <AnimatedSplash
      translucent={true}
      isLoaded={isLoaded}
      logoImage={require("./src/images/mkrd_removebg.png")}
      backgroundColor={"#fff"}
    >
     
      <AppNavigation />
    </AnimatedSplash>

  </>);
}
