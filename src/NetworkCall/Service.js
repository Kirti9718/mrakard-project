const baseUrl =
  'https://www.nileprojects.in/mkrad/';

//API END POINT LISTS

export const customer_name_list= 'api/v1/customers'
export const admin_login = 'api/v1/login'
export const crew_lead_name_list= 'api/v1/crew-lead'



export const requestGetApi = async (endPoint, body, method,token) => {
  var header = {};
  var url = baseUrl + endPoint;

  header = {'Content-type': 'application/json', 'Cache-Control': 'no-cache',
  'Authorization':'Bearer ' +token
};



  url = url + objToQueryString(body);
  console.log('Request Url:-' + url + '\n');

  try {
    let response = await fetch(url, {
      method: method,
      headers: header,
    });

    let code = await response.status;
    console.log(code);

    if (code == 200) {
      let responseJson = await response.json();
      console.log("data from back",responseJson);
      return {responseJson: responseJson, err: null, code: code};
    } else if (code == 400) {
      let responseJson = await response.json();
      return {responseJson: null, err: responseJson.message, code: code};
    } else {
      return {responseJson: null, err: 'Something went wrong!', code: code};
    }
  } catch (error) {
    return {
      responseJson: null,
      err: 'Something Went Wrong! Please check your internet connection.',
      code: 500,
    };
    console.error(error);
  }
};

export const requestPostApiMedia = async (endPoint, formData, method) => {
  var header = {
    'Content-type': 'multipart/form-data'
    
  };

  var url = baseUrl + endPoint;
  console.log('Request Url:-' + url + '\n');
  console.warn(formData + '\n');

  try {
    let response = await fetch(url, {
      method: method,
      body: formData,
      headers: header,
    });

    let code = await response.status;
    //  console.log(code )

    if (code == 200) {
      let responseJson = await response.json();
      console.log('data from backend', responseJson);
      return {responseJson: responseJson, err: null};
    } else if (code == 400) {
      let responseJson = await response.json();
      return {responseJson: null, err: responseJson.message};
    } else {
      return {responseJson: null, err: 'Something went wrong!'};
    }
  } catch (error) {
    return {
      responseJson: null,
      err: 'Something Went Wrong! Please check your internet connection.',
    };
    console.error(error);
  }
};

const objToQueryString = (obj) => {
  const keyValuePairs = [];
  for (const key in obj) {
    keyValuePairs.push(
      encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]),
    );
  }
  return keyValuePairs.length == 0 ? '' : '?' + keyValuePairs.join('&');
};
