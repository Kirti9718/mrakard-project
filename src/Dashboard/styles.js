import {StyleSheet} from 'react-native';


const styles = StyleSheet.create({



    CradContainer: {
      flex: 2,
      flexDirection: 'row',
      backgroundColor: '#fff',
      borderRadius: 15,
      shadowRadius: 30,
      borderWidth: 0,
      borderColor: '#ddd',
      borderBottomWidth: 0,
      shadowColor: '#000000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.9,
      shadowRadius: 3,
      elevation: 5,
  
    },
    MenuContainer: {
      marginLeft: 20,
      marginTop: 70,
      width: 30,
      height: 30,
  
  
    },
    MkradContainer: {
      width: 99,
      height: 70,
      marginTop: "15%",
      marginLeft: "37%",
      
    },
  
    RectangleContainer: {
      width: "65%",
      height: "28%",
      backgroundColor: '#3AB34A',
      alignSelf: 'center',
      borderRadius: 14,
      borderColor: '#ddd',
      shadowColor: '#000000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.9,
      shadowRadius: 3,
      elevation: 5,
      shadowRadius: 10,
      alignItems: 'center',
      position:'absolute',
      bottom:"-13%",
     
     
    },
  
    NotificationConrainer: {
      marginTop: "95%",
      marginRight: "8%"
    },
    Bottombar: {
      width: 40,
      height: 40,
      borderRadius: 40 / 2,
      marginTop: "13%",
      borderColor: '#ddd',
      borderWidth: 1,
      marginBottom: 43,
      alignItems: 'center',
      marginLeft: 30,
      marginRight: 32,
      padding: 7
    },
  
  
    JobItemContainer: {
  
      width: "85%",
      height: "20%",
      marginTop: "6%",
      backgroundColor: '#fff',
      alignSelf: 'center',
      borderRadius: 14,
      borderColor: '#ddd',
      shadowColor: '#000000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.9,
      shadowRadius: 3,
      elevation: 5,
      shadowRadius: 10,
      alignItems: 'center',
      flexDirection: 'row'
  
    },
  
    NotificationContainer: {
  
      width: "10%",
      height: "29%",
  
      alignSelf: 'center',
      borderRadius: 25,
      borderColor: '#3AB34A',
      borderWidth: 2,
      alignItems: 'center',
      marginLeft: "5%"
    },
  })
  
  
export default styles;  









