import React from 'react';
import {
  View, Text, TouchableOpacity,
  createDrawerNavigator, TextInput, Button, Image, StyleSheet, SafeAreaView
} from 'react-native'

import  styles from './styles'

class Dashboard extends React.Component {
  constructor(props) {
    super(props)

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.CradContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Image style={styles.MenuContainer}
              source={require('./../images/menu.png')} />
          </TouchableOpacity>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Image style={styles.MkradContainer}
              source={require('./../images/mkrd_removebg.png')} />
            <View style={styles.RectangleContainer}>
              <Text style={{ marginTop: 12, fontSize: 18, fontWeight: 'bold', color: '#fff' }}>Home</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('DashboardNotification')}>
            <Image style={styles.NotificationConrainer}
              source={require('./../images/notification.png')} />
          </TouchableOpacity>
        </View>

        <View style={{ flex: 5, flexDirection: 'column' }}>
          <Text style={{ fontWeight: 'bold', fontStyle: 'italic', fontSize: 17, color: '#201A4B', marginTop: "15%", marginLeft: "8%" }}>Good Morning !</Text>
          <Text style={{ fontWeight: 'bold', fontStyle: 'italic', fontSize: 20, color: '#000000', marginTop: "0%", marginLeft: "8%" }}>Jerry</Text>
          <View onStartShouldSetResponder={() => this.props.navigation.navigate("NewJobAssignScreen")} style={styles.JobItemContainer}>
            <View style={styles.NotificationContainer}>
              <Text style={{ fontSize: 14, fontStyle: 'italic', fontWeight: 'bold', color: '#3AB34A', }} >06</Text>
            </View>
            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989' }}>New job assigned</Text>
            <Image style={{ marginLeft: "24%" }} source={require('../images/next_arrow.png')}></Image>
          </View>



          <View onStartShouldSetResponder={() => this.props.navigation.navigate("OnPauseScreen")}  style={styles.JobItemContainer}>
            <View style={styles.NotificationContainer}>
              <Text style={{ fontSize: 14, fontStyle: 'italic', fontWeight: 'bold', color: '#3AB34A', }} >02</Text>
            </View>
            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989' }}>Paused job</Text>
            <Image style={{ marginLeft: "35%" }} source={require('../images/next_arrow.png')}></Image>
          </View>
        </View>
        <View style={{
          flex: .7, backgroundColor: '#222441', marginTop: "20%"
          , flexDirection: 'row', alignItems: 'center'
        }}>


          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("DashboardScreen")} style={{ alignItems: 'center', alignSelf: 'center', }} >
              <Image style={{ width: 20, height: 20, }} source={require('../images/home.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("ServiceReportScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 25, height: 25, }} source={require('../images/gear.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("CalenderScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 20, height: 20, }} source={require('../images/date.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Profile")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 20, height: 20, }} source={require('../images/profile.png')} />
            </TouchableOpacity>
          </View>
        </View>

      </View>
    )
  }
}



export default Dashboard


