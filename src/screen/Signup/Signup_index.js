import React, { useState } from 'react';
import {
    View, TextInput, Image, StyleSheet,
    Text,
    Keyboard,
    TouchableOpacity,
  
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Card } from 'react-native-shadow-cards';
import ImagePicker from 'react-native-image-picker';
import { ScrollView } from 'react-native-gesture-handler';





function SignUpPage(props) {
    console.log()
    let [userName, setUserName] = useState('');
    let [userEmail, setUserEmail] = useState('');
    let [userPassword, setUserPassword] = useState('');
    let [userNumber, setUserNumber] = useState('');
    let [filePath, setFilePath] = useState('');
    let [cca2, setcca2] = useState('');
    let [userReenterPassword, setUserRecenterPassword] = useState('');
   




    const OnbackClick = () => {
        props.navigation.goBack(null)
        // console.warn(props.navigation)
       // props.navigation.dispatch(NavigationActions.back())

    }

    const handleSubmitPress = () => {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // if (userName == '') {
        //     ToastAndroid.show("Please enter name ", ToastAndroid.SHORT);
        //     return;
        // }
        // if (reg.test(userEmail) != true) {
        //     ToastAndroid.show("Please valid email!", ToastAndroid.SHORT);
        //     return;
        // }
        // if (userNumber < 10) {
        //     ToastAndroid.show("Please enter number!", ToastAndroid.SHORT);
        //     return;
        // }
        // if (userPassword == '') {
        //     ToastAndroid.show("Please enter password!", ToastAndroid.SHORT);
        //     return;
        // }
        // if (userPassword.length<6) {
        //     ToastAndroid.show("Please enter at least 6 digit passoword!", ToastAndroid.SHORT);
        //     return;
        // }
        // if (userPassword!=userReenterPassword) {
        //     ToastAndroid.show("password does not match", ToastAndroid.SHORT);
        //     return;
        // }

        props.navigation.navigate('DashboardScreen')
      
    }

    const chooseFile = () => {
        const options = {
            title: 'Select Avatar',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        /**
         * The first arg is the options object for customization (it can also be null or omitted for default options),
         * The second arg is the callback which sends object: response (more info in the API Reference)
         */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                const source = { uri: 'data:image/jpeg;base64,' + response.data };
                setFilePath(source.uri)
            }
        });
    }

//   onPressFlag = () => {
//         countryPicker.openModal()
//     }

//     const selectCountry = (country) => {
//         userNumber.selectCountry(country.cca2.toLowerCase())
//         setUserNumber({ cca2: country.cca2 })
//     }
    return (

       
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ flex: .3 ,backgroundColor:'#E8E8E8',flexDirection:'row' }}>
                <TouchableOpacity activeOpacity={.5} onPress={OnbackClick} style={{
                    height: 30, marginLeft: "5%", marginTop: "4%", justifyContent: 'center',alignSelf:'center'
                }} >
                    <Image source={require('./../../images/back.png')} />
                </TouchableOpacity>
          
                <Image style={{
                    width:"30%", height: "42%",
                    alignSelf: "center", backgroundColor: '#f4f4f4f', marginTop:"4%",justifyContent: 'center',position:'absolute',left:"35%"
                  
                }} source={require('./../../images/mkrd_removebg.png')} />
            </View>

            <ScrollView style={{flex:2}}>

           
            <Card style={styles.CradContainer}>

            <View style={{position:'absolute',top:"-19%"}}>
                    <Text style={{ fontStyle: 'italic', fontWeight: 'bold', fontSize: 20, textAlign: 'center', marginBottom: 10 }}> Enter Basic Details</Text>
                </View>

                <View style={{position:'absolute',top:"-9%"}}>
                    <TouchableOpacity style={{
                        width: 110, height: 110, borderRadius: 110 / 2,

                    }} onPress={() => chooseFile()} >
                        {
                            filePath != '' ? <Image
                                source={{ uri: filePath }} style={{ width: 110, height: 110, borderRadius: 110 / 2 }}
                            /> : <View style={{
                                width: 110, height: 110, borderRadius: 110 / 2,
                                backgroundColor: '#266E7B',alignItems:'center'
                            }}>
                                <Image style={{marginTop:"30%"}} source={require('../../images/upload_img.png')}/>
                            </View>
                        }
                    </TouchableOpacity>
                </View>
      
                <View>
                    <Text style={{ fontStyle: 'italic', fontSize: 16 ,marginTop:"25%"}}> Upload Profile Image</Text>
                </View>
                <View style={styles.SectionStyle}>

                    
                    
  <Image style={{marginLeft:10,alignSelf:'center'}} source={require('../../images/name.png')}></Image>
                    
   
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={userName => setUserName(userName)}
                        underlineColorAndroid="#F6F6F7"
                        placeholder="Full Name"
                        placeholderTextColor="#000"
                        keyboardType='default'
                        returnKeyType="next"
                        onSubmitEditing={() => this._ageinput && this._ageinput.focus()}
                        blurOnSubmit={false}
                    />

                </View>


                <View style={styles.SectionStyle2}>

                    
                    
                    
  <Image style={{marginLeft:10,alignSelf:'center'}} source={require('../../images/email.png')}></Image>
                    
   
         
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={userEmail => setUserEmail(userEmail)}
                        underlineColorAndroid="#FFFFFF"
                        placeholder="Email ID" //12345
                        placeholderTextColor="#000"
                        keyboardType="email-address"
                        onSubmitEditing={Keyboard.dismiss}
                        blurOnSubmit={false}
                        secureTextEntry={true}
                    />
                </View>
                <View style={styles.SectionStyle2}>

                                 
                    
  <Image style={{marginLeft:10,alignSelf:'center',height:25,width:25}} source={require('../../images/phone.png')}></Image>
                    
   
                    <TextInput
                        style={styles.inputStyle2}
                        onChangeText={userNumber => setUserNumber(userNumber)}
                        underlineColorAndroid="#FFFFFF"
                        placeholder="Contact Number" //12345
                        placeholderTextColor="#000"
                        keyboardType='number-pad'
                        maxLength={10}
                        onSubmitEditing={Keyboard.dismiss}
                        blurOnSubmit={false}

                    />
                </View>
                <View style={styles.SectionStyle2}>

                                 
                    
  <Image style={{marginLeft:10,alignSelf:'center',height:25,width:25}} source={require('../../images/password.png')}></Image>
                    
   
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={userPassword => setUserPassword(userPassword)}
                        underlineColorAndroid="#FFFFFF"
                        placeholder="Enter Password" //12345
                        placeholderTextColor="#000"
                        keyboardType="default"
                        onSubmitEditing={Keyboard.dismiss}
                        blurOnSubmit={false}
                        secureTextEntry={true} />
                </View>
                <View style={styles.SectionStyle2}>

                                 
                    
  <Image style={{marginLeft:10,alignSelf:'center',height:25,width:25}} source={require('../../images/confirm_pass.png')}></Image>
                    
   
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={userReenterPassword => setUserRecenterPassword(userReenterPassword)}
                        underlineColorAndroid="#FFFFFF"
                        placeholder="Confirm Password" //12345
                        placeholderTextColor="#000"
                        keyboardType="default"
                        onSubmitEditing={Keyboard.dismiss}
                        blurOnSubmit={false}
                        secureTextEntry={true}
                    />
                </View>

                {/* <View style={styles.SectionStyle2}>
                    <PhoneInput style={styles.inputStyle}
                        ref={(ref) => { userNumber = ref; }}
                        onPressFlag={onPressFlag}
                    />
                    <CountryPicker
                        ref={(ref) => { countryPicker = ref; }}
                        onChange={(userNumber) => selectCountry(userNumber)}
                        translation='eng'
                        cca2={setcca2(cca2)} />
                </View> */}
                <TouchableOpacity
                    style={styles.buttonStyle}
                    activeOpacity={0.5}
                    onPress={handleSubmitPress}>
                    <Text style={styles.buttonTextStyle}>Submit</Text>
                </TouchableOpacity>
            </Card>
            
          
            </ScrollView>
        </SafeAreaView>
       
    );

}

const styles = StyleSheet.create({

    CradContainer: {
        marginTop: 110,
        marginLeft: 30,
        marginRight: 30,
        shadowRadius: 10,
        borderRadius: 20,
        width: 300,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        flex:2,
        marginBottom:"5%"

    }, SectionStyle: {

        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
        width:"80%",
    },

    SectionStyle2: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
        width:"80%",
    },



    buttonStyle: {
        backgroundColor: '#222441',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#222441',
        height: 50,
        width: 200,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 10,
        alignSelf: 'center',
        marginBottom:"20%",
        marginTop:"20%"


    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 12,
        fontSize: 16,
    },
    inputStyle: {
       marginLeft:"5%",
      alignSelf:'center'
    },

    inputStyle2: {
        marginLeft:"5%",
        alignSelf:'center'
    },

    inputStyle3: {
        flex: 1,
        color: '#379134',
        textDecorationLine: 'underline',
        fontStyle: 'italic',
        fontSize: 16,
        fontWeight: 'bold'

    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    }
})

export default SignUpPage