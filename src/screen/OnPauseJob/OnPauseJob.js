import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import styles from './styles'

class OnPauseScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            data: [
                { Job_request: 'lawn maintenance', Additional_job_notes: 'codes -652645', time_slot: '09:30 AM', customer_name: 'Jerry Paul' },
                { Job_request: 'Shop maintenance', Additional_job_notes: 'codes -122324', time_slot: '04:30 AM ', customer_name: 'Katty P' },
                { Job_request: 'cleaning', Additional_job_notes: 'codes -12355', time_slot: '05:30 AM', customer_name: 'Ronnie J' },
                { Job_request: 'lawn maintenance', Additional_job_notes: 'codes -67885', time_slot: '07:30 AM ', customer_name: 'Robby' },
                { Job_request: 'washing', Additional_job_notes: 'codes -22344', time_slot: '09:30 AM', customer_name: 'Kettu' },

            ]
        }
    }

    Separator = () => (
        <View style={styles.separator} />
    );
    OnbackClick = (props) => {
        // this.props.navigation.state.params.updateData(status);
        this.props.navigation.goBack()
        console.warn(props.navigation)
    }
    render() {
        return (

            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity activeOpacity={.5}
                        onPress={this.OnbackClick} style={styles.BackContainer} >
                        <Image source={require('../../images/back.png')} />
                    </TouchableOpacity>
                    <View style={styles.NotificationContainer}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'bold', color: '#3AB34A', }} >06</Text>
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: 18, marginLeft: 15 }}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'bold', color: '#898989' }}>Paused job</Text>
                    </View>
                </View>


                <View style={{ flex: 5 }}>
                    <FlatList data={this.state.data}
                        renderItem={({ item }) =>

                            <View style={styles.JobItemContainer}>

                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={styles.ImageContainer} source={require('../../images/backgroundImage.png')} />
                                    <Text style={styles.TextContainer_4}> {item.customer_name} </Text>

                                </View>
                                <this.Separator />
                                <Text style={styles.TextContainer_5}> Job Request</Text>
                                <Text style={styles.TextContainer_6}> {item.Job_request}</Text>
                                {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate('GetDirectionScreen')} style={{ position: 'absolute', right: "9%", top: "30%", width:"30%",height:"20%",}}>
                                    <Text style={styles.TextContainer_7}> Get direction</Text>
                                    <Image style={styles.TextContainer_8} source={require('../../images/directions_button.png')} />
                                </TouchableOpacity> */}
                                <this.Separator />

                                <Text style={styles.TextContainer_5}> Time </Text>
                                <Text style={styles.TextContainer_6}> {item.time_slot}</Text>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate('OnGoingJobScreen')} style={styles.TextContainer_9}>
                                    <Text style={styles.TextContainer_10}> Resume job</Text>
                                </TouchableOpacity>
                               

                            </View>
                        }
                    />
                </View>

            </View>)
    }
}



export default OnPauseScreen;