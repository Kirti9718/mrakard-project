import React from 'react';
import {
  View, Text, createAppContainer,
  createDrawerNavigator, TextInput, Button, Image, StyleSheet, TouchableOpacity
} from 'react-native'
import Modal from 'react-native-modal';

class ProfileScreen extends React.Component {
  constructor() {
    super();
    this.state={
      isModalVisible: false,
    }
  
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
};

  render() {

    return (
      <View style={{ flex: 1 }}>
        <View style={styles.CradContainer}>

          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>

            <Image style={styles.MenuContainer}
              source={require('../../images/menu.png')} />
          </TouchableOpacity>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Image style={styles.MkradContainer}
              source={require('../../images/mkrad_removbg.jpg')} />
            <View style={styles.RectangleContainer}>
              <Text style={{ marginTop: 12, fontSize: 18, fontWeight: 'bold', color: '#fff' }}>Profile</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('DashboardNotification')}>
            <Image style={styles.NotificationConrainer}
              source={require('../../images/notification.png')} />
          </TouchableOpacity>
        </View>


        <View style={styles.CradContainer2}>
          <View style={{ flex: 2 }}>
            <View style={{
              width: 90, height: 90,
              borderRadius: 90 / 2,
              position: "absolute",
              alignSelf: 'center',
              top: "-34%"
            }}>
              <Image style={{}} source={require('../../images/backgroundImage.png')} />
            </View>
            <Text style={{ marginTop: 10, fontStyle: 'italic', fontWeight: 'bold', fontSize: 20, position: 'absolute', top: "24%", alignSelf: 'center' }}>Jerry Paul </Text>
            <Text style={{ fontStyle: 'italic', fontSize: 16, color: '#3AB34A', position: 'absolute', bottom: "40%", alignSelf: 'center' }}> Crew lead</Text>

            <View style={{ width: 300, height: 40, backgroundColor: '#F4F4F4', alignSelf: 'center', position: 'absolute', bottom: "10%", }}>
              <Text style={{ fontStyle: 'italic', fontSize: 14, position: 'absolute', top: 1, alignSelf: 'center', left: "15%" }}>Jerry@gmaiol.com </Text>
              <Text style={{ fontStyle: 'italic', fontSize: 14, color: '#000000', position: 'absolute', left: "15%", top: 22 }}> +971836558877</Text>
            </View>
          </View>
        </View>



        <View style={{ flex: 2, }}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('EditProfile')}>
            <Text style={{ fontStyle: 'italic', fontSize: 18, alignSelf: 'center', marginTop: "5%", fontWeight: "bold" }}>Edit detail </Text></TouchableOpacity>

          <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChangePassword')}>
            <Text style={{ fontStyle: 'italic', fontSize: 18, alignSelf: 'center', color: '#000000', marginTop: "5%", fontWeight: "bold" }}> Change password</Text></TouchableOpacity>

            
            <Modal isVisible={this.state.isModalVisible}>
                            <View style={styles.JonMarked_Completed_Modal}>

                              <View style={{flex:1.5,marginTop:"15%"}}>
                                <Text style={styles.logoutText}>Are you sure</Text>
                                <Text style={styles.logoutText}>you want to logout?</Text>
                                </View>
                                <View style={{ flexDirection: 'row',flex:1 }}>                 
                               

                                <TouchableOpacity style={styles.OkBox} title="Hide modal" onPress={this.toggleModal} >
                                    <Text style={styles.CancelText}>Cancel</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={styles.OkBox} title="Hide modal" onPress={this.toggleModal} >
                                    <Text style={styles.OKText}>OK</Text>
                                </TouchableOpacity>
                            </View>
                            </View>
                        </Modal>

                      
          <TouchableOpacity onPress={this.toggleModal}>
            <Text style={{ fontStyle: 'italic', fontSize: 18, alignSelf: 'center', color: '#000000', marginTop: "5%", fontWeight: "bold" }}> Logout</Text></TouchableOpacity>

        </View>

        <View style={{
          flex: .7, backgroundColor: '#222441', marginTop: "20%"
          , flexDirection: 'row', alignItems: 'center'
        }}>

<View    style={styles.Bottombar}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Dashboard")} style={{ alignItems: 'center', alignSelf: 'center', }} >
              <Image style={{ width: 20, height: 20, }} source={require('../../images/home.png')} />
            </TouchableOpacity>
          </View>

          <View style={styles.Bottombar}>
            <TouchableOpacity  onPress={()=>this.props.navigation.navigate("ServiceReportScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 25, height: 25, }} source={require('../../images/gear.png')} />
            </TouchableOpacity>
          </View>

          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("CalenderScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 20, height: 20, }} source={require('../../images/date.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Profile")}   style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 20, height: 20, }} source={require('../../images/profile.png')} />
            </TouchableOpacity>
          </View>
        </View>
        

      </View>
    )
  }
}

const styles = StyleSheet.create({

  CradContainer: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 15,
    shadowRadius: 30,
    borderWidth: 0,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 5,

  },

  CancelText:{
   marginTop:"8%",
   color:'#fff',
   alignSelf:'center',
   fontSize:18,
   fontWeight:'bold'
    
  },

  OKText:{ 
   marginTop:"8%",
   color:'#fff',
   alignSelf:'center',
   fontSize:18,
   fontWeight:'bold'

  },
  OkBox:{
  width:"15%",
  height:"55%",
  borderRadius:15,
  backgroundColor:'#3AB34A',
  flex:1,
  alignItems:'center',
  marginLeft:10,
    marginRight:10,
  
  },
  

  CradContainer2: {
    flex: 2,
    flexDirection: 'column',
    backgroundColor: '#fff',
    borderRadius: 15,
    shadowRadius: 30,
    borderWidth: 0,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 5,
    marginTop: "25%",
    marginLeft: "10%",
    marginRight: "10%",
    alignItems: "center"

  },
  MenuContainer: {
    marginLeft: 20,
    marginTop: 70,
    width: 30,
    height: 30,


  },
  MkradContainer: {
    width: "34%",
    height: "40%",
    marginTop: "14%",
    marginLeft: "37%"
  },

  RectangleContainer: {
    width: "65%",
    height: "30%",
    backgroundColor: '#3AB34A',
    alignSelf: 'center',
    borderRadius: 14,
    borderColor: '#ddd',
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 5,
    shadowRadius: 10,
    alignItems: 'center',
    marginLeft: "5%",
    position:'absolute',
    bottom:"-13%"
    
  },

  NotificationConrainer: {
    marginTop: "85%",
    marginRight: "8%",
    width: 30,
    height: 30,
  },
  Bottombar: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    marginTop: "13%",
    borderColor: '#ddd',
    borderWidth: 1,
    marginBottom: 43,
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 32,
    padding: 7


  },
  JonMarked_Completed_Modal: {

    width: 300,
    height: 250,
    backgroundColor: '#fff',
     alignSelf: 'center',
    borderRadius: 14,
    borderColor: '#ddd',
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 5,
    shadowRadius: 10,
    marginBottom: 10,
   
   
  
    


},

logoutText:{

  alignSelf:'center',
  fontSize:18,
  
 


}
,
goutText:{

  alignSelf:'center',
  fontSize:18,
 


}



})




export default ProfileScreen


