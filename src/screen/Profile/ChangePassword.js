import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity,Image,TextInput,Keyboard, ToastAndroid } from 'react-native';
import { Card } from 'react-native-shadow-cards';
export default class ChangePassword extends React.Component {

    OnbackClick = () => {
        this.props.navigation.goBack()
      
    }

    SavePassword=()=>{
        ToastAndroid.show("Password change sucessfully !", ToastAndroid.SHORT);
    }
  render(){
    return (
        <View style={{ flex: 1 }}>
        <View style={styles.CradContainer}>
            <TouchableOpacity
                onPress={this.OnbackClick} style={styles.BackContainer} >
                <Image source={require('../../images/back.png')} />
            </TouchableOpacity>
            
            <View style={{ alignSelf: 'center', marginTop: 18, marginLeft: 15 }}>
                <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'bold', color: '#000' }}>Change Password</Text>
            </View>
        </View>

        <View style={{ flex: 2 }} >
        
        <View style={styles.CradContainer2}>

        <Text style={styles.TextContainer}>Old password</Text>


        <View style={styles.SectionStyle}>

                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={UserEmail => setUserEmail(UserEmail)}
                        underlineColorAndroid="#F6F6F7"
                        placeholder="Old paswword"
                        placeholderTextColor="#000"
                        keyboardType="email-address"
                        returnKeyType="next"
                        onSubmitEditing={() => this._ageinput && this._ageinput.focus()}
                        blurOnSubmit={false}
                    />
                </View>

 <Text style={styles.TextContainer2}>New password</Text>


                <View style={styles.SectionStyle}>
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={UserEmail => setUserEmail(UserEmail)}
                        underlineColorAndroid="#F6F6F7"
                        placeholder="New password"
                        placeholderTextColor="#000"
                        keyboardType="email-address"
                        returnKeyType="next"
                        onSubmitEditing={() => this._ageinput && this._ageinput.focus()}
                        blurOnSubmit={false}
                    />
                </View>

                
 <Text style={styles.TextContainer2}>Confirm Password</Text>


<View style={styles.SectionStyle}>
    <TextInput
        style={styles.inputStyle}
        onChangeText={UserEmail => setUserEmail(UserEmail)}
        underlineColorAndroid="#F6F6F7"
        placeholder="Confirm Password"
        placeholderTextColor="#000"
        keyboardType="email-address"
        returnKeyType="next"
        onSubmitEditing={() => this._ageinput && this._ageinput.focus()}
        blurOnSubmit={false}
    />
</View>

        </View>
          
    <TouchableOpacity onPress={this.SavePassword} style={styles.SaveContainer}>
        <Text style={styles.TextContainer1}>Save Changes</Text>
    </TouchableOpacity>
        </View>

      
      </View>
    );
  }
}


const styles = StyleSheet.create({
    CradContainer: {

        height: 120,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 15,
        shadowRadius: 30,
        borderWidth: 0,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,

    },

    CradContainer2: {

      
        flexDirection:'column',
        backgroundColor: '#fff',
        borderRadius: 15,
        shadowRadius: 30,
        borderWidth: 0,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        height:"60%",
        marginTop:"15%",
        marginLeft: "10%",
        marginRight: "10%",
        
    },
    
    BackContainer: {

        width: "25%",
        height: "20%",
        marginLeft: "5%",
        marginTop: "13%",
    },
    SectionStyle: {
        height: "12%",
        width: "80%",
        marginLeft: "8%",
        marginRight: "8%",
        marginTop:"5%",   
    },

    SectionStyle2: {
        height: 40,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,       
    },
    inputStyle: {
        color: '#000',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
    },
    TextContainer:{
       
        fontWeight:'bold',
        fontSize:16,
        marginTop:"15%",
        marginLeft: "8%"
    },

    TextContainer1:{
       
        fontWeight:'bold',
        fontSize:20,
        marginTop:"5%",
        color:'#fff'
        
    },
    TextContainer2:{
        fontWeight:'bold',
        fontSize:16,
        marginTop:"5%",
        marginLeft: "8%"
    },
    SaveContainer:{
        height:"8%",
        width:"40%",
        borderRadius:20,
        fontStyle:'normal',
        fontWeight:'bold',
        backgroundColor:'#379134',
        alignItems:'center',
        alignSelf:'center',
        marginTop:"8%"
        

    }
});