import React, { useState } from 'react';
import {
    View, TextInput, Button, Image, StyleSheet,
    Text, Alert,
    KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity,
    ScrollView,
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Card } from 'react-native-shadow-cards';


function LoginPage(props) {

    let [userEmail, setUserEmail] = useState('');
    let [userPassword, setUserPassword] = useState('');



    const handleSubmitPress = () => {
        //  setErrortext('');
        // if (userEmail=='') {
        //     alert('Please fill Email');
        //     return;
        // }
        // if (userPassword=='') {
        //     alert('Please fill Password');
        //     return;
        // }

        props.navigation.navigate('DashboardScreen')

        //props.navigation.navigate('TimeScreen')
    }


    const OnbackClick = () => {
        props.navigation.goBack(null)
        // console.warn(props.navigation)
        // props.navigation.dispatch(NavigationActions.back())

    }
    return (


        <SafeAreaView style={{ backgroundColor: '#fff' ,flex:1}} >
            <View style={{ flex: .5 ,backgroundColor:'#E8E8E8',flexDirection:'row' }}>
                <TouchableOpacity activeOpacity={.5} onPress={OnbackClick} style={{
                    height: "20%", marginLeft: "8%", marginTop: "8%", justifyContent: 'flex-end', alignItems: 'flex-start'
                }} >
                    <Image source={require('./../../images/back.png')} />
                </TouchableOpacity>
         
                <Image style={{
                    width: "35%", height: "28%",marginTop: "10%",
                    backgroundColor: '#f4f4f4f', marginLeft: "18%"
                }} source={require('./../../images/mkrd_removebg.png')} />
            </View>

            <Card style={styles.CradContainer}>

                <View style={styles.SectionStyle}>

  <Image style={{marginLeft:10,alignSelf:'center'}} source={require('../../images/email.png')}></Image>
                    
                    <TextInput
                 style={styles.inputStyle}
                        onChangeText={UserEmail => setUserEmail(UserEmail)}
                        underlineColorAndroid="#F6F6F7"
                        placeholder="Enter Email"
                        placeholderTextColor="#000"
                        keyboardType="email-address"
                        returnKeyType="next"
                        onSubmitEditing={() => this._ageinput && this._ageinput.focus()}
                        blurOnSubmit={false}
                    />
                </View>

                <View style={styles.SectionStyle2}>

                    
  <Image style={{marginLeft:10,alignSelf:'center'}} source={require('../../images/password.png')}></Image>
                    
   
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={UserPassword => setUserPassword(UserPassword)}
                        underlineColorAndroid="#FFFFFF"
                        placeholder="Enter Password" //12345
                        placeholderTextColor="#000"
                        keyboardType="default"
                        onSubmitEditing={Keyboard.dismiss}
                        blurOnSubmit={false}
                        secureTextEntry={true}
                    />
                </View>

                <View style={styles.SectionStyle3}>
                    <Text style={styles.inputStyle2}> Forget Password ?</Text>
                </View>

                <TouchableOpacity
                    style={styles.buttonStyle}
                    activeOpacity={0.5}
                    onPress={handleSubmitPress}>
                    <Text style={styles.buttonTextStyle}>LOGIN</Text>
                </TouchableOpacity>

            </Card>


            <View onStartShouldSetResponder={() => props.navigation.navigate('SignupScreen')} style={styles.SectionStyle4}>

                <Text style={styles.inputStyle3}> Don't have an account ? SIGNUP</Text>
            </View>

        </SafeAreaView>
    );

}








const styles = StyleSheet.create({

    CradContainer: {
        shadowRadius: 10,
        borderRadius: 20,
        height: "40%",
        width: "82%",
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position:'absolute',
        bottom:"20%",
        flex:1,
        
       
      

    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 60,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
        width:"80%"

    },

    SectionStyle2: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
        width:"80%",
        
    },

    SectionStyle3: {
        flexDirection: 'row',
        marginLeft: 85,
        marginTop: 10,
    },


    SectionStyle4: {
       
        position:'absolute',
        bottom:"8%",
        alignSelf:'center'
      
    },
    buttonStyle: {
        backgroundColor: '#3AB34A',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#3AB34A',
        height: 50,
        width: 200,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 70,
        alignSelf: 'center'


    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 12,
        fontSize: 16,
        alignSelf:'center',
        fontStyle:'normal'
    },
    inputStyle: {
       
      marginLeft:"5%",
      alignSelf:'center'
       
    },

    inputStyle2: {
        flex: 1,
        color: '#379134',
        textDecorationLine: 'underline',
        fontStyle: 'italic',
        fontSize: 16,
    },

    inputStyle3: {
      
        color: '#379134',
        textDecorationLine: 'underline',
        fontStyle: 'italic',
        fontSize: 16,
        fontWeight: 'bold'

    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    }
})

export default LoginPage