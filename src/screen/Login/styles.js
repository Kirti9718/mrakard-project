import {StyleSheet} from 'react-native'


const styles = StyleSheet.create(
    {
  
      container: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection:'column'
  
      },
      separator: {
        marginVertical: 5,
        borderBottomColor: '#fff',
        borderBottomWidth: StyleSheet.hairlineWidth,
      },
      LoginButtonStyle: {
  
        alignContent: "center",
        backgroundColor: '#3AB34A',
        borderRadius: 30,
        borderWidth: 3,
        borderColor: '#fff',
        width:250,
        height:"21%",
        
      },
  
      SignupButtonStyle: {
        width:250,
        backgroundColor: '#222441',
        borderRadius: 30,
        borderWidth: 3,
        borderColor: '#fff',
        height:"45%",

      },
  
      AdminButtonStyle: {
        resizeMode:'contain',
        backgroundColor:'#E8E8E8',
        flex:.5,
        marginBottom:"8%",
        height:"5%",
      
        
      },
  
      TextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 20,
        marginTop:"6%"
       
  
      },
      TextStyle2: {
        color: '#3AB34A',
        fontSize: 22,
        alignSelf:'center',
        fontStyle: 'italic',
        fontWeight:'bold',
        marginTop:"2%"
  
      }
    }
  )
  export default styles 