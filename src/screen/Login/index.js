import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import styles from '././styles'
import AsyncStorage from '@react-native-community/async-storage';




const Separator = () => (
  <View style={styles.separator} />
);


function LoginScreen(props) {


  let [admin_email,setAdminemail] = useState('');
  let[admin_password,setAdminPassword] = useState('');

 



  const AdminDashBoard =async()=>{

     try{
      admin_email= await AsyncStorage.getItem('admin_email');

      console.log('admin_email',admin_email);
     admin_password=await AsyncStorage.getItem('admin_password');
     }catch(error){

      console.log("error",error)

     }
   
    

    if(admin_email==null){
        props.navigation.navigate('AdminLoginPage')
        console.warn('admin_email',admin_email);
    }
    else{
      props.navigation.navigate('AdminDrawerNavs')
      console.warn('admin_email',admin_email);
    }

  //   AsyncStorage.getItem('admin_email').then((admin_email) => {
  //     this.props.navigation.navigate(admin_email ? 'AdminLoginPage' : 'AdminDrawerNavs')
  //  })
     //props.navigation.navigate('AdminLoginPage')
  }
  
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>

      <View style={{ flex: 1}}>
        <View style={{backgroundColor:'#E8E8E8',alignSelf: "center",flex:1.8}}>
          <Image style={{ width:"34%",height:"28%", marginTop: "15%", alignSelf: "center",position:'absolute'}} source={require('../../images/mkrad_removbg.jpg')} />
        </View>


        <View style={{ alignSelf: "center", marginTop: "25%" ,flex:3}} >

          <TouchableOpacity
            style={styles.LoginButtonStyle}
            activeOpacity={.5}
            onPress={() => props.navigation.navigate('LoginPage')}>
            <Text style={styles.TextStyle}> Login </Text>
          </TouchableOpacity>

          <Separator />

          <View >
            <TouchableOpacity
              style={styles.SignupButtonStyle}
              activeOpacity={.5}
              onPress={() => props.navigation.navigate('SignupScreen')}>
              <Text style={styles.TextStyle}> Signup </Text>
            </TouchableOpacity>

          </View>


     
        </View>

        <View style={styles.AdminButtonStyle}>
            <TouchableOpacity
              onPress={AdminDashBoard}>
              <Text style={styles.TextStyle2}> Administrator login </Text>
            </TouchableOpacity>

          </View>

      </View>


    </SafeAreaView>
  );
}


export default LoginScreen;
