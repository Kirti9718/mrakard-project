import React from 'react';
import { Button, View, Text, TouchableOpacity, Image, FlatList ,Linking, Platform} from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import styles from './OnGoingJob_styles'
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker'
let imageList=[]
let CameraImageList=[]

class OnGoingJobScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            data: [
                { Job_request: 'lawn maintenance', Job_Location: '142 Victoria Court, Fort Kent, ME, Maine-04743', time_slot: '09:30 AM to 10:30 AM', customer_name: 'Jerry Paul' },
              
            ],
            isModalVisible: false, //state of modal default false 
            isModalPauseVisible: false,
            filePath: '',
            imageurl: '',
            ImagePathFromGallery:[] ,
        
        }
    }



    dialCall = () => {

        let phoneNumber = '';
    
        if (Platform.OS === 'android') {
          phoneNumber = 'tel:${+1234567890}';
        }
        else {
          phoneNumber = 'telprompt:${+1234567890}';
        }
    
        Linking.openURL(phoneNumber);
      };
    Separator = () => (
        <View style={styles.separator} />
    );
    OnbackClick = (props) => {
        // this.props.navigation.state.params.updateData(status);
        this.props.navigation.goBack()
        console.warn(props.navigation)
    }

    ShowMaxAlert = (EnteredValue) => {
        //    var TextLength = EnteredValue.length.toString() ;

    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    toggleModal2 = () => {
        this.setState({ isModalPauseVisible: !this.state.isModalPauseVisible });

    };


    // takePicture = async () => {
    //      this.setState({ isModalVisible: !this.state.isModalVisible });
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options);
    //         console.log(data.uri);
    //     }
    // }

    OpenOnPauseScreeen = () => {
        // this.setState({ isModalVisible: !this.state.isModalVisible });
        this.props.navigation.navigate('OnPauseScreen')
        this.setState({ isModalPauseVisible: !this.state.isModalPauseVisible });


    }




    chooseFile = () => {

        const options = {
            title: 'Select Avatar',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
            
                const source = { uri: 'data:image/jpeg;base64,' + response.data };

                        
              imageList.push(source);

              console.log("imagelist:::::::::",imageList)

              this.setState({ImagePathFromGallery: imageList })    
             
              console.log("imagelist:::::::::",this.state.ImagePathFromGallery)

            }
        });
    }

    openCamera = () => {
        this.props.navigation.navigate('CameraScreen')
        this.setState({ isModalVisible: !this.state.isModalVisible });

    }

    async componentDidMount() {
        let { imageUrl } = await this.props.route.params
        // console.warn('hghsgswg')
        console.log("testing......", imageUrl)
        this.setState({ filePath: `file://${imageUrl}` })
        console.log('imagepath', `file://${imageUrl}`)
    }
    removeImage=(index)=>{
        imageList.splice(index,1)
         console.warn('kirti.....')
        this.setState({ImagePathFromGallery: imageList }) 
    }

    showAllItems=(item, index)=> {

 
        console.log("url",item)
        return (
            <View style={{marginLeft:10}}>
                <Image style={{ width:80, height: 60 ,marginTop:10,marginLeft:10}}
      source={{uri:item.uri}}></Image>

<TouchableOpacity style={{position:'absolute',}} onPress={()=>this.removeImage(index)}>
      <Image style={{ width: 30, height: 30,}} source={require('../../images/cross.png')}></Image>
  </TouchableOpacity>

               
            </View>
        )

    }
    // uri: this.state.filePath
    render() {
        const { navigate } = this.props.navigation;


      
        //   const tittle=this.props.route.params
        //imageurl=this.props.route.params

        //  console.log('krti..........',imageurl)


        return (


            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity
                        onPress={this.OnbackClick} style={styles.BackContainer} >
                        <Image source={require('../../images/back.png')} />
                    </TouchableOpacity>
                    <View style={styles.NotificationContainer}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'normal', color: '#3AB34A', marginBottom: 8 }} >06</Text>
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: 18, marginLeft: 15 }}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'bold', color: '#898989' }}>Ongoing job</Text>
                    </View>
                </View>

                <ScrollView>
                    <View style={{ flex: 4 }}>


                        <FlatList data={this.state.data}
                            renderItem={({ item }) =>

                                <View style={styles.JobItemContainer}>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={styles.ImageContainer} source={require('../../images/backgroundImage.png')} />
                                        <Text style={styles.TextContainer_4}> {item.customer_name} </Text>

                                    </View>
                                    <this.Separator />
                                    <Text style={styles.TextContainer_5}> Job Request</Text>
                                    <Text style={styles.TextContainer_6}> {item.Job_request}</Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('GetDirectionScreen')} style={{ position: 'absolute', right: "10%", top: "39%", }}>
                                        <Text style={styles.TextContainer_7}> Get direction</Text>
                                        <Image style={styles.TextContainer_8} source={require('../../images/directions_button.png')} />
                                    </TouchableOpacity>
                                    <this.Separator />

                                    <Text style={styles.TextContainer_5}> Time slot</Text>
                                    <Text style={styles.TextContainer_6}> {item.time_slot}</Text>
                                    {/* <TouchableOpacity onPress={ this.dialCall} style={styles.TextContainer_9}>
                                        <Text style={styles.TextContainer_10}> Call</Text>
                                        <Image  style={{marginLeft:10,width:30,height:30}} source={require('../../images/ring_phone.png')}></Image>
                                    </TouchableOpacity> */}
                                    <this.Separator />
                                    <Text style={styles.TextContainer_11}>Job Location </Text>
                                    <Text style={styles.TextContainer_12}> {item.Job_Location}</Text>
                                    <Image style={styles.LocationContainer} source={require('../../images/location.png')} />

                                </View>
                            }
                        />
                    </View>

                    <View style={{ flex: 3 }} >
                        <View style={styles.JobNotes}>
                            <Text style={{ marginTop: 5, marginLeft: "5%", fontStyle: 'italic' }}>Job note</Text>
                            <Text style={{ color: '#FF0000', position: 'absolute', left: "22%", top: 2 }}>*</Text>
                            <TextInput style={{ flex: 1, marginLeft: 10, }}
                                multiline={true}
                                numberOfLines={6}
                                onChange={EnteredValue => this.ShowMaxAlert(EnteredValue)}>
                            </TextInput>
                            <Text style={styles.TextContainer}>Maximum 500 words</Text>
                        </View>
                    </View>

                    <View style={{ flex: 1 }} >


                        <TouchableOpacity activeOpacity={.5} onPress={this.toggleModal} style={styles.JonMarked_Completed}>
                            <Text style={styles.TextContainer_2}>Mark job completed</Text>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={.5} onPress={this.toggleModal2}>
                            <Text style={styles.Pause_Job}>Pause the job</Text>
                        </TouchableOpacity>

                        <Modal isVisible={this.state.isModalVisible}>
                            <View style={styles.JonMarked_Completed_Modal}>
                                <Text style={styles.TextContainer_13}>Upload completed job images</Text>

                                <ScrollView >
                                    <FlatList style={{ marginLeft: 20, marginTop: 30, marginRight: 10 }}
                                     data={this.state.ImagePathFromGallery}

                                        keyExtractor={(_, index) => index.toString()}
                                        extraData={this.state.data}
                                       horizontal={true}
                                        renderItem={({ item, index }) =>
                                        
                                            this.showAllItems(item, index)
                                        }
                                    />
                                </ScrollView>
                                <View style={{ flexDirection: 'row',alignSelf:'center' }}>


                                    <TouchableOpacity style={styles.capture} style={styles.RecatngleBox} onPress={this.chooseFile}>
                                        <Text style={{ position: 'absolute', top: "8%", color: '#000000' }}>Click image</Text>
                                        <Image style={{ position: 'absolute', bottom: "20%", }} source={require('../../images/camera.png')}></Image>
                                    </TouchableOpacity>

                                    {/* <TouchableOpacity style={styles.RecatngleBox}
                                        onPress={this.chooseFile}>
                                        <Text style={{ position: 'absolute', top: "8%", color: '#000000' }}>Upload image</Text>
                                        <Image style={{ position: 'absolute', bottom: "20%", }} source={require('../../images/upload_icon.png')}></Image>
                                    </TouchableOpacity> */}
                                </View>

                                <TouchableOpacity style={styles.JonMarked_Completed_2} title="Hide modal" onPress={this.toggleModal} >
                                    <Text style={styles.TextContainer_2}>Mark as completed</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>

                        <Modal isVisible={this.state.isModalPauseVisible}>
                            <View style={styles.JonMarked_Completed_Modal}>
                                <Text style={styles.TextContainer_13}>Enter reason for pausing job</Text>

                                <View style={styles.PauseJobNotes}>
                                    <Text style={{ marginTop: 5, marginLeft: "5%", fontStyle: 'italic' }}>Job note</Text>
                                    <Text style={{ color: '#FF0000', position: 'absolute', left: "24%", top: 2 }}>*</Text>
                                    <TextInput style={{  marginLeft: 10,position:'absolute',top:"-5%" }}
                                        multiline={true}
                                        numberOfLines={6}
                                        onChange={EnteredValue => this.ShowMaxAlert(EnteredValue)}>
                                    </TextInput>
                                    <Text style={styles.TextContainer}>Maximum 500 words</Text>
                                </View>
                                <TouchableOpacity style={styles.PausejobButton} title="Hide modal" onPress={this.OpenOnPauseScreeen}  >
                                    <Text style={styles.TextContainer_2}>Paused job</Text>
                                </TouchableOpacity>


                            </View>
                        </Modal>


                    </View>





                </ScrollView>



            </View>

        )
    }
}




export default OnGoingJobScreen;