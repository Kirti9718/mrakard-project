import React from 'react';
import {
    View, Text,
    TextInput, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity
} from 'react-native'

import styles from './styles'

let arrayholder = [];
let newData;

export default class AssignJobListOnDate extends React.Component {


    state = {
        search: '',
    };
    constructor({ navigation }) {
        super({ navigation });
        this.state = {

            text: '',
            data: [
                { customer_name: 'Akash', job_name: 'lawn maintenance', },
                { customer_name: 'Riya', job_name: 'Tree and shrub planting', },
                { customer_name: 'Rahul', job_name: 'Claening', },
                { customer_name: 'Rony', job_name: 'lawn maintenance', },
                { customer_name: 'Ram', job_name: 'lawn maintenance', },
                { customer_name: 'Shyam', job_name: 'lawn maintenance', },
            ],
            search: false,
            resultData: [],
        }
    }
    searchData(text) {
        arrayholder = [...this.state.data];
        if (text.length > 0) {
            this.setState({ search: true })
        } else {
            this.setState({ search: false })
        }

        let textData = text.toUpperCase();
        newData = arrayholder.filter(item => item.job_name.toUpperCase().includes(textData))
        console.log("result==>", newData)
    }

    OnbackClick = (props) => {
        this.props.navigation.goBack()
        console.warn(props.navigation)
    }


    Separator = () => (
        <View style={styles.separator} />
    );

  

    render() {

        // const { navigation } = this.props;  
        // const user_name = navigation.getParam('date', 'NO-User');  
     
        // console.log("PROPS " + user_name);
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity onPress={this.OnbackClick} style={styles.BackContainer}>
                        <Image 
                            source={require('../../images/back.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Text style={styles.DateContainer}>15 October 2020</Text>
                           
                    </View>
                    </View>


                <View style={{ flex: 5, }}>
                    <ScrollView>
                        <View style={styles.tileContainer}>

                            <View style={{flex:.5}}>
                        <Image style={styles.reverseBtn}
                            source={require('../../images/reverse_btn.png')} />
                    </View>
                    <View style={{flex:1}}>
                        <Text style={styles.tileText}>15 October 2020</Text>
                        </View>
                        <View style={{flex:.5}}>
                        <Image style={styles.forwordBtn}
                            source={require('../../images/forward_btn.png')} />
                    </View>
                          
                        </View>


                        <FlatList data={this.state.search ? newData : this.state.data}
                            renderItem={({ item }) =>
                                <View onStartShouldSetResponder={()=>this.props.navigation.navigate('OnUpComingJobScreen')} style={styles.JobItemContainer}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 50, height: 50, marginTop: 15, marginLeft: "10%", flexWrap: 'wrap' }} source={require('../../images/backgroundImage.png')} />
                                        <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 15, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#000' }}> {item.customer_name} </Text>
                                    </View>
                                    <this.Separator />
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", alignSelf: 'center', marginTop: "4%", fontStyle: 'italic', color: '#898989' }}>Job request </Text>
                                        <Image style={{ marginTop: 10, marginLeft: "52%" }} source={require('../../images/next_arrow.png')}></Image>
                                    </View>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989' }}> {item.job_name}</Text>
                                </View>



                            }
                            keyExtractor={(_, index) => index.toString()}
                        />

                    </ScrollView>
                </View>






                </View>
        )
    }
}





