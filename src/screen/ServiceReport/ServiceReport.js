import React from 'react';
import {
    View, Text,
    TextInput, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity
} from 'react-native'

import styles from './styles'

let arrayholder = [];
let newData;

export default class ServiceReportScreen extends React.Component {


    state = {
        search: '',
    };
    constructor({ navigation }) {
        super({ navigation });
        this.state = {

            text: '',
            data: [
                { customer_name: 'Riya', job_name: 'Gardening', },
                { customer_name: 'Akash', job_name: 'Cleaning', },
                { customer_name: 'kirti', job_name: 'HouseKeeping', },
                { customer_name: 'Neah', job_name: 'Gardening', },
                { customer_name: 'Radheshyam', job_name: 'Cleaning', },
                { customer_name: 'Shiwa', job_name: 'HouseKeeping', },
            ],
            search: false,
            resultData: [],
        }
    }
    searchData(text) {
        arrayholder = [...this.state.data];
        if (text.length > 0) {
            this.setState({ search: true })
        } else {
            this.setState({ search: false })
        }

        let textData = text.toUpperCase();
        newData = arrayholder.filter(item => item.job_name.toUpperCase().includes(textData))
        console.log("result==>", newData)
    }



    Separator = () => (
        <View style={styles.separator} />
    );


    render() {
        const { search } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Image style={styles.MenuContainer}
                            source={require('../../images/menu.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Image style={styles.MkradContainer}
                            source={require('../../images/mkrad_removbg.jpg')} />
                        <View style={styles.RectangleContainer}>
                            <Text style={{ marginTop: 12, fontSize: 18, fontWeight: 'bold', color: '#fff' }}>Service Report</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DashboardNotification')}>
                        <Image style={styles.NotificationConrainer}
                            source={require('../../images/notification.png')} />
                    </TouchableOpacity>
                </View>


                <View style={{ flex: 5, }}>
                    <ScrollView>
                        <View style={styles.SearchContainer}>
                            <TouchableOpacity>
                                <Image style={{ width: 30, height: 30, margin: 12 }} source={require('../../images/search.png')} />
                            </TouchableOpacity>
                            <TextInput
                                placeholder="Search by name"
                                onChangeText={(text) => this.searchData(text)}
                                keyboardType='default'
                                blurOnSubmit={false}
                            />
                        </View>


                        <FlatList data={this.state.search ? newData : this.state.data}
                            renderItem={({ item }) =>
                                <View onStartShouldSetResponder={()=>this.props.navigation.navigate('OnCompleteJobScreen')} style={styles.JobItemContainer}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 50, height: 50, marginTop: 15, marginLeft: "10%", flexWrap: 'wrap' }} source={require('../../images/backgroundImage.png')} />
                                        <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 15, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#000' }}> {item.customer_name} </Text>
                                    </View>
                                    <this.Separator />
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", alignSelf: 'center', marginTop: "4%", fontStyle: 'italic', color: '#898989' }}>Job request </Text>
                                        <Image style={{ marginTop: 10, marginLeft: "52%" }} source={require('../../images/next_arrow.png')}></Image>
                                    </View>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989' }}> {item.job_name}</Text>
                                </View>



                            }
                            keyExtractor={(_, index) => index.toString()}
                        />

                    </ScrollView>
                </View>






                <View style={{
                    flex: .7, backgroundColor: '#222441', marginTop: "20%"
                    , flexDirection: 'row', alignItems: 'center'
                }}>

                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() =>this.props.navigation.navigate("Dashboard")} style={{ alignItems: 'center', alignSelf: 'center', }} >
                            <Image style={{ width: 20, height: 20, }} source={require('../../images/home.png')} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("ServiceReportScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 25, height: 25, }} source={require('../../images/gear.png')} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("CalenderScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 20, height: 20, }} source={require('../../images/date.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Profile")} style={{ alignItems: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 20, height: 20, }} source={require('../../images/profile.png')} />
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }
}





