import React, { useEffect, useState } from 'react';
import {
    View, TextInput, Button, Image, StyleSheet,
    Text, Alert,
    KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity,
    ScrollView,
    ToastAndroid,
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Card } from 'react-native-shadow-cards';
import AsyncStorage from '@react-native-community/async-storage';
import { requestPostApiMedia, admin_login } from '../NetworkCall/Service'
function AdminLoginPage(props) {

    let [userEmail, setUserEmail] = useState('');
    let [userPassword, setUserPassword] = useState('');
    let [loading, setLoading] = useState(false);
    let [errortext, setErrortext] = useState('');

    const handleSubmitPress = async () => {
        //  setErrortext('');
        if (userEmail == '') {
            ToastAndroid.show('Please fill Email', ToastAndroid.LONG);
            return;
        }
        if (userPassword == '') {
            ToastAndroid.show('Please fill Password', ToastAndroid.LONG);
            return;
        }

        setLoading(true);
        try {
            await AsyncStorage.setItem('admin_email', userEmail);
        } catch (error) {
            console.log('error::::::', error)
        }

        const formData = new FormData()
        formData.append('email', userEmail)
        formData.append('password', userPassword)

        const { responseJson, err } = await requestPostApiMedia(admin_login, formData, 'POST')
        console.log("Response of Admin +++++++++++++++++++++++++++", responseJson)

        if (responseJson.status) {
            AsyncStorage.setItem('token_key', responseJson.access_token)
            console.log('token_key::::::::::::', responseJson.access_token);
            props.navigation.navigate('AdminDrawerNavs')
        }


    }

    const OnbackClick = () => {
        props.navigation.goBack(null)
    
    }
    return (


        <SafeAreaView style={{ backgroundColor: '#fff', flex: 1 }} >
            <View style={{ flex: .5, backgroundColor: '#E8E8E8', flexDirection: 'row' }}>
                <TouchableOpacity activeOpacity={.5} onPress={OnbackClick} style={{
                    width: "26%", height: "20%", marginLeft: "8%", marginTop: "8%", justifyContent: 'flex-end', alignItems: 'flex-start'
                }} >
                    <Image source={require('../images/back.png')} />
                </TouchableOpacity>

                <Image style={{
                    width: "35%", height: "28%", marginTop: "10%",
                    backgroundColor: '#f4f4f4f'
                }} source={require('../images/mkrd_removebg.png')} />
            </View>

            <Card style={styles.CradContainer}>

                <View style={styles.SectionStyle}>



                    <Image style={{ marginLeft: 10, alignSelf: 'center' }} source={require('../images/email.png')}></Image>

                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={UserEmail => setUserEmail(UserEmail)}
                        underlineColorAndroid="#F6F6F7"
                        placeholder="Enter Email"
                        placeholderTextColor="#000"
                        keyboardType="email-address"
                        returnKeyType="next"
                        onSubmitEditing={() => this._ageinput && this._ageinput.focus()}
                        blurOnSubmit={false}
                    />
                </View>

                <View style={styles.SectionStyle2}>



                    <Image style={{ marginLeft: 10, alignSelf: 'center' }} source={require('../images/password.png')}></Image>


                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={UserPassword => setUserPassword(UserPassword)}
                        underlineColorAndroid="#FFFFFF"
                        placeholder="Enter Password" //12345
                        placeholderTextColor="#000"
                        keyboardType="default"
                        onSubmitEditing={Keyboard.dismiss}
                        blurOnSubmit={false}
                        secureTextEntry={true}
                    />
                </View>

                <View style={styles.SectionStyle3}>
                    <Text style={styles.inputStyle2}> Forget Password ?</Text>
                </View>

                <TouchableOpacity
                    style={styles.buttonStyle}
                    activeOpacity={0.5}
                    onPress={handleSubmitPress}>
                    <Text style={styles.buttonTextStyle}>LOGIN</Text>
                </TouchableOpacity>

            </Card>




        </SafeAreaView>
    );

}








const styles = StyleSheet.create({

    CradContainer: {
        shadowRadius: 10,
        borderRadius: 20,
        height: "40%",
        width: "82%",
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: "20%",
        flex: 1,




    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 60,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
        width: "80%"
    },

    SectionStyle2: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#379134',
        width: "80%",
    },

    SectionStyle3: {
        flexDirection: 'row',
        marginLeft: 85,
        marginTop: 10,
    },


    SectionStyle4: {

        position: 'absolute',
        bottom: "8%",
        alignSelf: 'center'

    },
    buttonStyle: {
        backgroundColor: '#3AB34A',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#3AB34A',
        height: 50,
        width: 200,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 70,
        alignSelf: 'center'


    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 12,
        fontSize: 16,
        alignSelf: 'center',
        fontStyle: 'normal'
    },
    inputStyle: {
        marginLeft: "5%",
        alignSelf: 'center'
    },

    inputStyle2: {
        flex: 1,
        color: '#379134',
        textDecorationLine: 'underline',
        fontStyle: 'italic',
        fontSize: 16,
    },

    inputStyle3: {

        color: '#379134',
        textDecorationLine: 'underline',
        fontStyle: 'italic',
        fontSize: 16,
        fontWeight: 'bold'

    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    }
})

export default AdminLoginPage