import React from 'react';
import {
    View, Text, TouchableOpacity,
    createDrawerNavigator, TextInput, Button, Image, StyleSheet, SafeAreaView
} from 'react-native'
import CalendarPicker from 'react-native-calendar-picker';
import { DrawerActions } from '@react-navigation/native';

let startDate;
class AdminHome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisiblebtn: true,
            selectedStartDate: null,
        };
        this.onDateChange = this.onDateChange.bind(this);

    }

    toggleStatus() {
        this.setState({
            isVisiblebtn: !this.state.isVisiblebtn
        });
        //console.log('toggle button handler: ' + this.state.isVisiblebtn);
    }

    onDateChange(date) {
        this.setState({
          selectedStartDate: date,
        
          
        });
      
        this.props.navigation.navigate('AdminJobScheduleList', { date: startDate}
        
        )

        console.warn(startDate);
      }

    render() {
        const { selectedStartDate } = this.state;
        startDate = selectedStartDate ? selectedStartDate.toString() : '';
 
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity onPress={() =>this.props.navigation.openDrawer()}>
                        <Image style={styles.MenuContainer}
                            source={require('../images/menu.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Image style={styles.MkradContainer}
                            source={require('../images/mkrad_removbg.jpg')} />
                        <View onStartShouldSetResponder={() => this.props.navigation.navigate('AdminJobScheduleList')} style={styles.RectangleContainer}>
                            <Text style={{ marginTop: 10, fontSize: 18, fontWeight: 'bold', color: '#fff' }}>Calendar view</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AdminNotification')}>
                        <Image style={styles.NotificationConrainer}
                            source={require('../images/notification.png')} />
                    </TouchableOpacity>
                </View>


                <View style={styles.container}>
                    <CalendarPicker

                        onDateChange={this.onDateChange}
                    />
                </View>
                {this.state.isVisiblebtn == false ?
                    <View style={styles.WeeklyButton} onStartShouldSetResponder={() => this.toggleStatus()} >
                        <Image style={{ marginLeft: 20 }} source={require('../images/scheduler.png')} />
                        <Text style={{ marginLeft: 10, color: '#3AB34A', fontSize: 18, fontWeight: 'normal' }}>Weekly</Text>

                    </View>

                    :

                    <View style={styles.WeeklyButton} onStartShouldSetResponder={() => this.toggleStatus()} >
                        <Image style={{ marginLeft: 20 }} source={require('../images/scheduler.png')} />
                        <Text style={{ marginLeft: 10, color: '#3AB34A', fontSize: 18, fontWeight: 'normal' }}>Monthly</Text>
                    </View>
                }

                <View style={{
                    flex: .7, backgroundColor: '#222441', marginTop: "20%"
                    , flexDirection: 'row', alignItems: 'center'
                }}>

                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("AdminHome")} style={{ alignItems: 'center', alignSelf: 'center', }} >
                            <Image style={{ width: 20, height: 20, }} source={require('../images/home.png')} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("SchedularScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 20, height: 20, }} source={require('../images/date.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("CrewManagement")} style={{ alignItems: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 25, height: 25, }} source={require('../images/teamwork.png')} />
                        </TouchableOpacity>
                    </View>


                    <View style={styles.Bottombar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("AdminProfileScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
                            <Image style={{ width: 20, height: 20, }} source={require('../images/profile.png')} />
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    CradContainer: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 15,
        shadowRadius: 30,
        borderWidth: 0,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,

    },
    MenuContainer: {
        marginLeft: 20,
        marginTop: 70,
        width: 30,
        height: 30,


    },
    MkradContainer: {
        width: "35%",
        height: "39%",
        marginTop: "15%",
        marginLeft: "37%"
    },

    RectangleContainer: {
        width: "65%",
        height: "28%",
        backgroundColor: '#3AB34A',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        alignItems: 'center',
        position: 'absolute',
        bottom: "-13%"
    },

    NotificationConrainer: {
        marginTop: "95%",
        marginRight: "8%"
    },
    Bottombar: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        marginTop: "13%",
        borderColor: '#ddd',
        borderWidth: 1,
        marginBottom: 43,
        alignItems: 'center',
        marginLeft: 30,
        marginRight: 32,
        padding: 7
    },


    JobItemContainer: {

        width: "85%",
        height: "20%",
        marginTop: "6%",
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        alignItems: 'center',
        flexDirection: 'row'

    },

    NotificationContainer: {
        width: "10%",
        height: "29%",
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#3AB34A',
        borderWidth: 2,
        alignItems: 'center',
        marginLeft: "5%"
    },
    container: {
        flex: 4,
        backgroundColor: '#FFFFFF',
        marginTop: "15%",
    },

    WeeklyButton: {
        flexDirection: 'row',
        flex: .5,
        alignSelf: 'center',
        position: 'absolute',
        bottom: "12%", width: "33%",
        height: '6%',
        borderRadius: 20,
        borderColor: '#3AB34A',
        borderWidth: 1,
        alignItems: 'center'
    }
})




export default AdminHome


