import React from 'react';
import {
    View, Text,
    TextInput, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity, Picker
} from 'react-native'

import styles from './home_styles'

import Modal from 'react-native-modal';
import DateTimePickerModal from "react-native-modal-datetime-picker";



let oneTimeSelected_time
let oneTimeSelected_date
let monthNames = ["Jan", "Feb", "March", "April", "May", "June",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"
];
export default class AdminJobScheduleList extends React.Component {


    state = {
        search: '',
    };
    constructor({ navigation }) {
        super({ navigation });
        this.state = {

            text: '',
            data: [
                { customer_name: 'Akash', job_name: 'lawn maintenance', },
                { customer_name: 'Riya', job_name: 'Tree and shrub planting', },
                { customer_name: 'Rahul', job_name: 'Claening', },
                { customer_name: 'Rony', job_name: 'lawn maintenance', },
                { customer_name: 'Ram', job_name: 'lawn maintenance', },
                { customer_name: 'Shyam', job_name: 'lawn maintenance', },
            ],
            resultData: [],
            isModalVisible: false,
            isModalVisible2: false,
            choosenIndex: 0,
            isTimePickerVisible: false,
            oneTimeSelected_time: "",
            isDatePickerVisible: false,
            oneTimeSelected_date: ""
        }
    }

    OnbackClick = (props) => {
        this.props.navigation.goBack()
    }
    Separator = () => (
        <View style={styles.separator} />
    );

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    toggleModal2 = () => {
        this.setState({ isModalVisible2: !this.state.isModalVisible2 });
    };
    OpenOnCancelScreeen = () => {
        this.props.navigation.navigate('AdminCancelJob')
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true });
    };
    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false })
    }
    showTimePicker = () => {
        this.setState({ isTimePickerVisible: true });
    };

    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false })
    }

    handleTimePicked = (time) => {
        let AM_PM;
        if (time.getHours() < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        oneTimeSelected_time = time.getHours() + ':' + time.getMinutes() + " " + AM_PM;
        this.setState({ oneTimeSelected_time })
        this.hideTimePicker();

    };
    handleDatePicked = (date) => {
        oneTimeSelected_date = date.getDate() + " " + monthNames[(date.getMonth())] + " ," + date.getFullYear();
        this.setState({ oneTimeSelected_date })
        this.hideDatePicker();
    };
    render() {
        const { search } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity onPress={this.OnbackClick} style={styles.BackContainer}>
                        <Image
                            source={require('../images/back.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Text style={styles.DateContainer}>15 October 2020</Text>

                    </View>
                </View>


                <View style={{ flex: 5, }}>
                    <ScrollView>
                        <View style={styles.tileContainer}>

                            <View style={{ flex: .5 }}>
                                <Image style={styles.reverseBtn}
                                    source={require('../images/reverse_btn.png')} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.tileText}>15 October 2020</Text>
                            </View>
                            <View style={{ flex: .5 }}>
                                <Image style={styles.forwordBtn}
                                    source={require('../images/forward_btn.png')} />
                            </View>

                        </View>


                        <FlatList data={this.state.data}
                            renderItem={({ item }) =>
                                <View style={styles.createjobschedule}>
                                    <View onStartShouldSetResponder={() => this.props.navigation.navigate('AdminOnGoingJob')} style={styles.JobItemContainer}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image style={{ width: 50, height: 50, marginTop: 15, marginLeft: "10%", flexWrap: 'wrap' }} source={require('../images/backgroundImage.png')} />
                                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 15, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#000' }}> {item.customer_name} </Text>
                                        </View>

                                        <View style={{ flexDirection: 'row', height: "20%", backgroundColor: '#F2F2F2', marginTop: "5%", alignItems: 'center' }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#898989' }}>Assigned to crew leader :</Text>
                                            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "1%", fontStyle: 'italic', color: '#898989' }}>John smith</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#898989' }}>Job request </Text>
                                            <Image style={{ marginTop: 10, marginLeft: "52%" }} source={require('../images/next_arrow.png')}></Image>
                                        </View>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989', position: 'absolute', bottom: "13%" }}> {item.job_name}</Text>
                                    </View>



                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity activeOpacity={.5} onPress={this.toggleModal}><Text style={styles.TextJobCreate}>Reschedule</Text></TouchableOpacity>
                                        {this.Separator}
                                        <TouchableOpacity activeOpacity={.5} onPress={this.toggleModal2} ><Text style={styles.TextJobCreate}>Change crew lead</Text></TouchableOpacity>

                                    </View>

                                    <Modal isVisible={this.state.isModalVisible} backdropOpacity={0.1}>
                                        <View style={styles.JonMarked_Completed_Modal}>
                                            <Text style={styles.TextContainer_13}>Reschedule job</Text>
                                            <TouchableOpacity style={styles.RectangleContainer_5} onPress={this.showDatePicker} >
                                                <TextInput editable={false} placeholder="select date" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_date} ></TextInput>
                                                <DateTimePickerModal
                                                    isVisible={this.state.isDatePickerVisible}
                                                    mode="date"
                                                    onConfirm={this.handleDatePicked}
                                                    onCancel={this.hideDatePicker}
                                                    forment="dd-MM-y"


                                                />
                                                <Image style={{ alignSelf: 'center', marginLeft: "35%", width: 20, height: 20 }} source={require('../images/date_picker.png')} />

                                            </TouchableOpacity>

                                            <TouchableOpacity style={styles.RectangleContainer_5} onPress={this.showTimePicker} >
                                                <TextInput editable={false} placeholder="select time" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_time} ></TextInput>

                                                <DateTimePickerModal
                                                    isVisible={this.state.isTimePickerVisible}
                                                    mode="time"
                                                    onConfirm={this.handleTimePicked}
                                                    onCancel={this.hideTimePicker}
                                                    forment="dd-MM-y"
                                                    amPmAriaLabel="Select AM/PM"
                                                    is24Hour={false}


                                                />
                                                <Image style={{ marginLeft: "35%", width: 30, height: 30 }} source={require('../images/time.png')} />

                                            </TouchableOpacity>
                                         
                                            <TouchableOpacity activeOpacity={.5} style={styles.SaveButton} title="Hide modal" onPress={this.toggleModal}  >
                                              
                                                <Text style={styles.TextContainer_2}>Save</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity activeOpacity={.5} title="Hide modal" onPress={this.OpenOnCancelScreeen}  >
                                                <Text style={styles.cancelJob}>Cancel job</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Modal>

                                    <Modal isVisible={this.state.isModalVisible2} backdropOpacity={0.0}>
                                        <View style={styles.JonMarked_Completed_Modal}>
                                            <Text style={styles.TextContainer_13}>Change crew lead</Text>

                                            <TouchableOpacity style={styles.RectangleContainer_3}  >
                                                <Picker style={styles.PickerContainer}
                                                    selectedValue={this.state.language4}
                                                    onValueChange={(itemValue4, itemPosition4) =>
                                                        this.setState({ language4: itemValue4, choosenIndex: itemPosition4 })}
                                                >
                                                    <Picker.Item label="John smith" value="java" />
                                                    <Picker.Item label="Kitty" value="js" />
                                                    <Picker.Item label="Robin" value="rn" />

                                                </Picker>
                                            </TouchableOpacity>
                                            <TouchableOpacity activeOpacity={.5} style={styles.SaveButton} title="Hide modal" onPress={this.toggleModal2}  >
                                                <Text style={styles.TextContainer_2}>Reassign</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Modal>



                                </View>


                            }
                            keyExtractor={(_, index) => index.toString()}
                        />


                    </ScrollView>
                </View>
            </View>
        )
    }
}





