import React from 'react';
import { map, Picker, Alert, TextInput, CheckBox, View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'

import styles from './Newstyles'
import Modal from 'react-native-modal';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import AsyncStorage from '@react-native-community/async-storage';
import { customer_name_list, crew_lead_name_list, requestGetApi } from '../../../NetworkCall/Service'


let token_key
let selectedDate
let oneTimeSelected_date
let oneTimeSelected_time
let oneTimeSelected_time_2
let monthNames = ["Jan", "Feb", "March", "April", "May", "June",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"
];

class NewCreateJob extends React.Component {


    constructor({ navigation }) {
        super({ navigation });

        this.state = {
            choosenIndex: 0,
            isSelected: false,
            isRepeatJobSelected: false,
            isModalVisible: false,
            customerIndex: 0,
            isDatePickerVisible: false,
            oneTimeSelected_date: "",
            isTimePickerVisible: false,
            oneTimeSelected_time: "",
            oneTimeSelected_time_2: "",
            isTimePickerVisible_2: false,
            customerlist_name: [],
            crewdlist_name:[],
            customerName_selectedValue:'',
            crewName_selectedValue:''
        }


    }


    async componentDidMount() {
        this.CustomerListApi()


    }

    CustomerListApi = async () => {
   let   token  = await AsyncStorage.getItem('token_key');
        const body = {
        }
        const { responseJson, err } = await requestGetApi(customer_name_list, body, 'GET', token)
        console.log("customer_name_list Response------------", responseJson)
        if (responseJson.status) {
            this.CrewListApi()
            const { data } = responseJson.data
            console.log("data---->>", data.length)

            let name_arr = []
            for (let i = 0; i < data.length; i++) {

                let select_customer_name = 'select customer name'
                let new_data = data[i].name;
                console.log('customerlist--------:::::::::::::', new_data);
                name_arr.push(new_data)
            }
            this.setState({ customerlist_name: name_arr })
            console.log("NewData============", this.state.customerlist_name)
        }

    }

    CrewListApi = async () => {
      let  token = await AsyncStorage.getItem('token_key');
        const body = {
        }
        const { responseJson, err } = await requestGetApi(crew_lead_name_list, body, 'GET', token)
        console.log("crew_lead_name_list Response------------", responseJson)
        if(responseJson.status){
            const{data}=responseJson.data
            console.log("data>>>>>>>>>>>>>",data.length)

            let name_arr = []

            for (let i = 0; i < data.length; i++) {

                let select_customer_name = 'select customer name'
                let new_data = data[i].name;
                console.log('crewlist--------:::::::::::::', new_data);
                name_arr.push(new_data)
            }
            this.setState({ crewdlist_name: name_arr })
            console.log("NewData============", this.state.crewdlist_name)
        }

    }


    OnbackClick = (props) => {
        this.props.navigation.goBack()
    }
    Separator = () => (
        <View style={styles.separator} />
    );

    handleChange = () => {
        this.setState({ isSelected: !this.state.isSelected })
    }
    handleChangeOnRepeatJob = () => {
        this.setState({ isRepeatJobSelected: !this.state.isRepeatJobSelected })
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    toggleModal_2 = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
        this.props.navigation.navigate('AdminJobScheduleList');
    };
    ShowMaxAlert = (EnteredValue) => { }
    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true });
    };
    showTimePicker = () => {
        this.setState({ isTimePickerVisible: true });
    };

    showTimePicker_2 = () => {
        this.setState({ isTimePickerVisible_2: true });
    };

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false })
    }

    handleDatePicked = (date) => {
        oneTimeSelected_date = date.getDate() + " " + monthNames[(date.getMonth())] + " ," + date.getFullYear();
        this.setState({ oneTimeSelected_date })
        this.hideDatePicker();
    };

    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false })
    }
    hideTimePicker_2 = () => {
        this.setState({ isTimePickerVisible_2: false })
    }
    handleTimePicked = (time) => {
        let AM_PM;
        if (time.getHours() < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        oneTimeSelected_time = time.getHours() + ':' + time.getMinutes() + " " + AM_PM;
        this.setState({ oneTimeSelected_time })
        this.hideTimePicker();
    };
    handleTimePicked_2 = (time) => {
        let AM_PM;
        if (time.getHours() < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }
        oneTimeSelected_time_2 = time.getHours() + ':' + time.getMinutes() + " " + AM_PM;
        this.setState({ oneTimeSelected_time_2 })
        console.warn('A time has been picked: ', oneTimeSelected_time);
        this.hideTimePicker_2();
    };
    render() {

        return (
            <View style={{ flex: 1 }} >
                <View style={{ flex: .8 }}>
                    <View style={styles.CradContainer}>
                        <TouchableOpacity onPress={this.OnbackClick} style={styles.BackContainer}>
                            <Image
                                source={require('../../../images/back.png')} />
                        </TouchableOpacity>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={styles.NotificationContainer}>
                                <Text style={{ fontSize: 16, fontStyle: 'italic', fontWeight: 'normal', color: '#3AB34A', marginBottom: 8 }} >06</Text>
                            </View>
                            <Text style={styles.DateContainer}>Create new Job schedule</Text>

                        </View>
                    </View>
                </View>

                <View style={{ flex: 3 }}>

                    <ScrollView style={styles.CreateJobContainer}>
                        <Text style={styles.TextContainer_1}>Select job</Text>
                        <TouchableOpacity style={styles.RectangleContainer}  >
                            <Picker style={styles.PickerContainer}
                                selectedValue={this.state.language}
                                onValueChange={(itemValue, itemPosition) =>
                                    this.setState({ language: itemValue, choosenIndex: itemPosition })}
                            >
                                <Picker.Item label="lawn maintenance" value="java" />
                                <Picker.Item label="Cleaning" value="js" />
                                <Picker.Item label="Garding maintenance" value="rn" />

                            </Picker>
                        </TouchableOpacity>
                        <View style={styles.checkboxContainer}>
                            <CheckBox
                                value={this.state.isSelected}
                                onValueChange={this.handleChange}
                                style={styles.checkbox}
                            />
                            <Text style={styles.label}>One time</Text>
                        </View>

                        <View style={styles.checkboxContainer2}>
                            <CheckBox
                                value={this.state.isRepeatJobSelected}
                                onValueChange={this.handleChangeOnRepeatJob}
                                style={styles.checkbox}
                            />
                            <Text style={styles.label}>Repeat job</Text>
                        </View>
                        {
                            this.state.isRepeatJobSelected == false ?
                                <View>
                                    <Text style={styles.TextContainer_1}>Select date</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_2} onPress={this.showDatePicker} >
                                        {/* <Text style={{ alignSelf: 'center', marginLeft: "15%", }}>{oneTimeSelected_date}</Text> */}
                                        <TextInput editable={false} placeholder="select date" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_date} ></TextInput>
                                        <DateTimePickerModal
                                            isVisible={this.state.isDatePickerVisible}
                                            mode="date"
                                            onConfirm={this.handleDatePicked}
                                            onCancel={this.hideDatePicker}
                                            forment="dd-MM-y"


                                        />
                                        <Image style={{ alignSelf: 'center', marginLeft: "35%", width: 20, height: 20 }} source={require('../../../images/date_picker.png')} />

                                    </TouchableOpacity>

                                    <Text style={styles.TextContainer_1}>Select customer</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_3}  >
                                        {<Picker style={styles.PickerContainer}
                                            selectedValue={this.state.selectedValue}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue })} >
                                            {this.state.customerlist_name.map((item, key) =>
                                                <Picker.Item label={item} value={item} key={key} />
                                            )}
                                        </Picker>}
                                    </TouchableOpacity>


                                    <Text style={styles.TextContainer_1}>Select Crew lead</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_3}  >
                                    {<Picker style={styles.PickerContainer}
                                            selectedValue={this.state.selectedValue}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue })} >
                                            {this.state.crewdlist_name.map((item,key)=>
                                              <Picker.Item label={item} value={item} key={key} />
                                            )
                                              
                                            }
                                        </Picker>}
                                    </TouchableOpacity>


                                    <Text style={styles.TextContainer_1}>Time slot</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_3} onPress={this.showTimePicker} >
                                        <TextInput editable={false} placeholder="select time" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_time} ></TextInput>
                                        <DateTimePickerModal
                                            isVisible={this.state.isTimePickerVisible}
                                            mode="time"
                                            onConfirm={this.handleTimePicked}
                                            onCancel={this.hideTimePicker}
                                            forment="dd-MM-y"
                                            amPmAriaLabel="Select AM/PM"
                                            is24Hour={false}
                                        />
                                        <Image style={{ marginLeft: "35%", width: 30, height: 30 }} source={require('../../../images/time.png')} />

                                    </TouchableOpacity>

                                    <Text style={styles.TextContainer_1}>to</Text>


                                    <TouchableOpacity style={styles.RectangleContainer_3} onPress={this.showTimePicker_2} >

                                        <TextInput editable={false} placeholder="select time" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_time_2} ></TextInput>
                                        <DateTimePickerModal
                                            isVisible={this.state.isTimePickerVisible_2}
                                            mode="time"
                                            onConfirm={this.handleTimePicked_2}
                                            onCancel={this.hideTimePicker_2}
                                            forment="dd-MM-y"
                                            amPmAriaLabel="Select AM/PM"
                                            is24Hour={false}
                                            maxDetail={oneTimeSelected_time}

                                        />
                                        <Image style={{ marginLeft: "35%", width: 30, height: 30 }} source={require('../../../images/time.png')} />

                                    </TouchableOpacity>
                                </View>
                                :

                                <View>
                                    <View style={{ backgroundColor: '#D7F0DB', marginTop: 15 }}>
                                        <View style={styles.checkboxContainer_1}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>Every Week On The Same Day of the Week</Text>
                                        </View>

                                        <View style={styles.checkboxContainer_1}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>Every Other Week On the Same Day of the Week</Text>
                                        </View>
                                        <View style={styles.checkboxContainer_1}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>Every Other Month on the Same Day of the Month</Text>
                                        </View>
                                        <View style={styles.checkboxContainer_1}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>Every Quarter on the Same Day of the Month</Text>
                                        </View>
                                        <Text style={styles.label_4}>Duration</Text>

                                        <View style={styles.checkboxContainer_1}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <View style={styles.label_3}>
                                                <TextInput style={{ color: '#000' }} placeholder="Others" />

                                            </View>
                                        </View>

                                    </View>

                                    <Text style={styles.TextContainer_2}>Select date</Text>

                                    <TouchableOpacity style={styles.RectangleContainer_5} onPress={this.showDatePicker} >
                                        {/* <Text style={{ alignSelf: 'center', marginLeft: "15%", }}>{oneTimeSelected_date}</Text> */}
                                        <TextInput editable={false} placeholder="select date" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_date} ></TextInput>
                                        <DateTimePickerModal
                                            isVisible={this.state.isDatePickerVisible}
                                            mode="date"
                                            onConfirm={this.handleDatePicked}
                                            onCancel={this.hideDatePicker}
                                            forment="dd-MM-y"


                                        />

                                        <Image style={{ alignSelf: 'center', marginLeft: "35%", width: 20, height: 20 }} source={require('../../../images/date_picker.png')} />
                                    </TouchableOpacity>

                                    <Text style={styles.TextContainer_1}>Select customer</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_4}  >
                                    {<Picker style={styles.PickerContainer}
                                            customerName_selectedValue={this.state.customerName_selectedValue}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ customerName_selectedValue: itemValue })} >
                                            {this.state.customerlist_name.map((item, key) =>
                                                <Picker.Item label={item} value={item} key={key} />
                                            )}
                                        </Picker>}
                                    </TouchableOpacity>


                                    <Text style={styles.TextContainer_1}>Select Crew lead</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_4}  >
                                    {<Picker style={styles.PickerContainer}
                                            crewName_selectedValue={this.state.crewName_selectedValue}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ crewName_selectedValue: itemValue })} >
                                            {this.state.crewdlist_name.map((item,key)=>
                                              <Picker.Item label={item} value={item} key={key} />
                                            )
                                              
                                            }
                                        </Picker>}
                                    </TouchableOpacity>




                                    <View style={{ backgroundColor: '#D7F0DB', marginTop: 10 }}>

                                        <Text style={styles.job_reminder}>Crew start Job Reminders need to be:</Text>
                                        <View style={styles.checkboxContainer2}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>1 Day Prior</Text>
                                        </View>
                                        <View style={styles.checkboxContainer2}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>3 Days Prior</Text>
                                        </View>
                                        <View style={styles.checkboxContainer2}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>5 days Prior</Text>
                                        </View>

                                        <View style={styles.checkboxContainer2}>
                                            <CheckBox
                                                value={this.state.isSelected}
                                                onValueChange={this.handleChange}
                                                style={styles.checkbox}
                                            />
                                            <Text style={styles.label_2}>15 minutes Prior</Text>
                                        </View>

                                    </View>

                                    <Text style={styles.TextContainer_1}>Time slot</Text>
                                    <TouchableOpacity style={styles.RectangleContainer_4} onPress={this.showTimePicker} >
                                        <TextInput editable={false} placeholder="select time" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_time} ></TextInput>
                                        <DateTimePickerModal
                                            isVisible={this.state.isTimePickerVisible}
                                            mode="time"
                                            onConfirm={this.handleTimePicked}
                                            onCancel={this.hideTimePicker}
                                            forment="dd-MM-y"
                                            amPmAriaLabel="Select AM/PM"
                                            is24Hour={false}
                                        />
                                        <Image style={{ marginLeft: "35%", width: 30, height: 30 }} source={require('../../../images/time.png')} />

                                    </TouchableOpacity>

                                    <Text style={styles.TextContainer_1}>to</Text>


                                    <TouchableOpacity style={styles.RectangleContainer_4} onPress={this.showTimePicker_2} >

                                        <TextInput editable={false} placeholder="select time" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_time_2} ></TextInput>
                                        <DateTimePickerModal
                                            isVisible={this.state.isTimePickerVisible_2}
                                            mode="time"
                                            onConfirm={this.handleTimePicked_2}
                                            onCancel={this.hideTimePicker_2}
                                            forment="dd-MM-y"
                                            amPmAriaLabel="Select AM/PM"
                                            is24Hour={false}
                                            maxDetail={oneTimeSelected_time}

                                        />
                                        <Image style={{ marginLeft: "35%", width: 30, height: 30 }} source={require('../../../images/time.png')} />

                                    </TouchableOpacity>

                                </View>}

                        <View style={styles.JobNotes}>
                            <Text style={{ marginTop: 5, marginLeft: "5%", fontStyle: 'italic' }}>Additional job notes</Text>
                            <TextInput style={{ flex: 1, marginLeft: 10, }}
                                multiline={true}
                                numberOfLines={6}
                                onChange={EnteredValue => this.ShowMaxAlert(EnteredValue)}>
                            </TextInput>

                        </View>

                        <TouchableOpacity style={styles.CreateButton} onPress={this.toggleModal}>
                            <Text style={{ fontStyle: 'normal', fontWeight: 'bold', color: '#fff', fontSize: 18, marginTop: 10 }}>Create</Text>
                        </TouchableOpacity>

                        <Modal isVisible={this.state.isModalVisible}>
                            <View style={styles.JonMarked_Completed_Modal}>

                                <Image source={require('../../../images/checked.png')} style={{ alignSelf: 'center', marginTop: "5%" }} />
                                <Text style={styles.TextContainer_13}>Job schedule successfully</Text>
                                {/* <Text style={styles.TextContainer_13}>Successfully</Text> */}

                                <View style={{ flexDirection: 'row' }}>


                                </View>

                                <TouchableOpacity style={styles.CreateButton} title="Hide modal" onPress={this.toggleModal_2} >
                                    <Text style={styles.TextContainer_3}>View job</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>




                    </ScrollView>
                </View>
            </View>
        )
    }
}








export default NewCreateJob;