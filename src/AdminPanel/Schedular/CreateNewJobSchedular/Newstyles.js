import {StyleSheet} from 'react-native'



const styles = StyleSheet.create({

   

    CradContainer: {
       
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 15,
        shadowRadius: 30,
        borderWidth: 0,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        flex:2
        
       
      
       

    },
    BackContainer: {
        width: "20%",
        height: "10%",
        marginLeft: "5%",
        marginTop: "17%",

    },
     

    NotificationContainer: {

        width: "12%",
        height: "15%",
        marginTop: "8%",
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#3AB34A',
        borderWidth: 1,
        alignItems: 'center',
    },
    DateContainer: {
        marginTop: "24%",
        fontSize:18,
        fontWeight:'bold',
        color:'#898989',
        fontStyle:'italic',marginLeft:"5%"
       
    },
   



  
   
    RectangleContainer: {
        width: "70%",
         height:"20%",
        marginTop: "3%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%",
        height:45 
    },

    
    RectangleContainer_3: {
        width: "70%",
         height:"10%",
        marginTop: "3%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%",   
        height:45,
        flexDirection:'row'
    },

    RectangleContainer_4: {
        width: "70%",
        
        marginTop: "3%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%",   
        flexDirection:'row'  ,
      
        height:45

    },

    RectangleContainer_5: {
        width: "70%",
        marginTop: "3%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%",   
        flexDirection:'row'  ,
    
        height:45
       

    },


    
    RectangleContainer_2: {
        width: "70%",
        marginTop: "3%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%",
        flexDirection:'row',
        height:45
       
         
    },

    PickerContainer:{height: 35, 
        width: "90%",
        alignSelf:'center',
        marginLeft:"5%", },

    NotificationConrainer: {
        marginTop: "95%",
        marginRight: "8%"
    },
   

    JobItemContainer: {

        width: 350,
        height: 200,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#fff',
        shadowColor: '#fff',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 3,
        marginBottom: 20

    },

    JobItemContainer: {

        width: 350,
        height: 200,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#fff',
        shadowColor: '#fff',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 3,
        marginBottom: 20

    },

   
    separator: {
        marginTop: 12,
        borderLeftWidth: 1,
        borderLeftColor: '#000',
      
    },
   


    CreateButton:{
        backgroundColor: '#3AB34A',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#222441',
        height: 50,
        width: 250,
        alignItems: 'center',
        borderRadius: 30,
        alignSelf: 'center',
        position:'absolute',
        bottom:"5%"
        

    },
    CreateJobContainer_2: {

        width: 350,
        height: 970,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        marginBottom: 30,
        marginTop:"10%",
    },
   
    separator: {
        marginTop: 12,
        borderLeftWidth: 1,
        borderLeftColor: '#000',
      
    },
   


    CreateButton:{
        backgroundColor: '#3AB34A',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#222441',
        height: 50,
        width: 250,
        alignItems: 'center',
        borderRadius: 30,
        alignSelf: 'center',
        marginTop:20,
        marginBottom:40
    
       
  

      
        

        

    },
    CreateJobContainer: {

        width: 350,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        marginBottom: 10,
        marginTop:"10%",
        paddingVertical:20,
       
       
       
     
        
      
    },

    TextContainer_1:{
        marginTop:"5%",
        marginLeft:"15%",
        color:'#898989',
      
        

    },

    TextContainer_2:{
        marginTop:"12%",
        marginLeft:"15%",
        color:'#898989',
        

    },
    container: {
        flex: 1,
        justifyContent: "center",
        marginLeft :30
      },
      checkbox: {
        alignSelf: "center",
        color:'#3AB34A',
        borderColor:'#3AB34A',
        borderWidth:2,
       
        
      },
      label: {
        margin: 8,
      },
      label_2: {
        marginTop: 8,
        marginRight:"25%",
        marginLeft: "3%",

      },

      label_4: {
        marginTop: 8,
        marginLeft: "17%",
        fontSize:18,
        fontWeight:'bold'

      },

      label_3: {
        marginTop: 10,
        marginLeft: "4%",
        height:38,
        width:"45%",
        borderColor:'#000',
        borderRadius:20,
        borderWidth:1,
        marginBottom:10,alignItems:'center'

      },
      checkboxContainer: {
        flexDirection: "row",
        marginLeft: "15%", 
        marginTop: "4%",  
        flex:1         
      },

      checkboxContainer2: {
        flexDirection: "row",
        marginLeft: "15%", 
        flex:1,
        
                 
      },

      checkboxContainer_1: {
        flexDirection: "row",
        marginLeft: "15%", 
       
                 
      },
      JonMarked_Completed_Modal: {

        width: 340,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        marginBottom: 10,
        position: 'absolute',
        bottom: "-1%",
        


    },
    
    TextContainer_13: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: "10%",
        fontStyle: 'italic',
        color: '#000',
        alignSelf: 'center'

    },
    TextContainer_13: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: "2%",
        fontStyle: 'italic',
        color: '#000',
        alignSelf: 'center'

    },

    TextContainer_3:{
        marginTop:"5%",
        color:'#fff',
        alignSelf:'center',
        fontSize:18,
        fontWeight:'bold'
        

    },
    JobNotes: {

        width: 300,
        height: 130,
        marginTop: "6%",
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#3AB34A',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        marginBottom: 10,
        borderWidth:1
    },

    job_reminder:{
        fontSize:18,
        fontWeight:'bold',
        color:'#000',
        fontStyle:'italic',
        marginLeft:"12%",
        marginBottom:"1%",
        marginTop:"3%"

    }
    
 


})

export default styles