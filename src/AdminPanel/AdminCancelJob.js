import React from 'react';
import {
    View, Text,
    TextInput, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity
} from 'react-native'

import styles from './home_styles'

let arrayholder = [];
let newData;
import Modal from 'react-native-modal';
export default class AdminCancelJob extends React.Component {


    state = {
        search: '',
    };
    constructor({ navigation }) {
        super({ navigation });
        this.state = {

            text: '',
            data: [
                { customer_name: 'Akash', job_name: 'lawn maintenance', },
                { customer_name: 'Riya', job_name: 'Tree and shrub planting', },
                { customer_name: 'Rahul', job_name: 'Claening', },
                { customer_name: 'Rony', job_name: 'lawn maintenance', },
                { customer_name: 'Ram', job_name: 'lawn maintenance', },
                { customer_name: 'Shyam', job_name: 'lawn maintenance', },
            ],
            resultData: [],
         
        }
    }
 
    OnbackClick = (props) => {
        this.props.navigation.goBack()
        console.warn(props.navigation)
    }


    Separator = () => (
        <View style={styles.separator} />
    );

   

    render() {
        const { search } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity onPress={this.OnbackClick} style={styles.BackContainer}>
                        <Image 
                            source={require('../images/back.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={styles.NotificationContainer}>
                        <Text style={{ fontSize: 16, fontStyle: 'italic', fontWeight: 'normal', color: '#3AB34A', marginBottom: 8 }} >06</Text>
                    </View>
                        <Text style={styles.DateContainer}>Cancel job</Text>
                           
                    </View>
                    </View>


                <View style={{ flex: 5, }}>
                                                            <FlatList data={ this.state.data}
                            renderItem={({ item }) =>
                      
                                <View onStartShouldSetResponder={()=>this.props.navigation.navigate('AdminJobDetails')} style={styles.CancelJobContainer}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 50, height: 50, marginTop: 15, marginLeft: "10%", flexWrap: 'wrap' }} source={require('../images/backgroundImage.png')} />
                                        <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 15, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#FF0000' }}> {item.customer_name} </Text>
                                    </View>
                                    
                                    <View style={{ flexDirection: 'row' ,height:"20%",backgroundColor:'#F2F2F2',marginTop:"5%",alignItems:'center'}}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", alignSelf: 'center', fontStyle: 'italic', color: '#898989' }}>Assigned to crew leader :</Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "1%", fontStyle: 'italic', color: '#898989' }}>John smith</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", alignSelf: 'center',fontStyle: 'italic', color: '#898989' }}>Job request </Text>
                                        <Image style={{ marginTop: 10, marginLeft: "52%" }} source={require('../images/next_arrow.png')}></Image>
                                    </View>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989',position:'absolute',bottom:"13%" }}> {item.job_name}</Text> 
                                </View>                                
                            }
                            keyExtractor={(_, index) => index.toString()}
                        />
                      
                </View>
                </View>
        )
    }
}





