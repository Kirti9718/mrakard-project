import { StyleSheet } from 'react-native'


const styles = StyleSheet.create({



    CradContainer: {
        flex: 1.4,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 15,
        shadowRadius: 30,
        borderWidth: 0,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,

    },
    MenuContainer: {
        marginLeft: 20,
        marginTop: 70,
        width: 30,
        height: 30,


    },
    BackContainer: {
        width: "20%",
        height: "10%",
        marginLeft: "5%",
        marginTop: "17%",

    },
    
    NotificationContainer: {

        width: "12%",
        height: "15%",
        marginTop: "8%",
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#3AB34A',
        borderWidth: 1,
        alignItems: 'center',
    },

    DateContainer: {
        marginTop: "24%",
        fontSize:18,
        fontWeight:'bold',
        color:'#898989',
        fontStyle:'italic',marginLeft:"5%"
       
    },
    MkradContainer: {
        width: "30%",
        height: "42%",
        marginTop: "12%",
        marginLeft: "37%",
       

    },
    DateContainer: {
        marginTop: "24%",
        fontSize:18,
        fontWeight:'bold',
        color:'#898989',
        fontStyle:'italic',marginLeft:"5%"
       
    },

    RectangleContainer: {
        width: 200,
        height: 50,
       
        backgroundColor: '#3AB34A',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        alignItems: 'center',
        marginLeft: "5%",
        position:'absolute',
        bottom:"-13%"
       

    },

    NotificationConrainer: {
        marginTop: "95%",
        marginRight: "8%"
    },
    Bottombar: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        marginTop: "13%",
        borderColor: '#ddd',
        borderWidth: 1,
        marginBottom: 43,
        alignItems: 'center',
        marginLeft: 30,
        marginRight: 32,
        padding: 7
    },


    JobItemContainer: {

        width: 350,
        height: 100,
        marginTop: "6%",
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        marginBottom: 10





    },
   

    SearchContainer: {
        width: 350,
        height: 50,
        marginTop: "10%",
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        flexDirection: 'row',

    },
  
   

})
export default styles