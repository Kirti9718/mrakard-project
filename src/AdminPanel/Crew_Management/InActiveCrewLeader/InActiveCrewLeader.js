import React from 'react';
import {
    View, Text,
    TextInput, Image, StyleSheet, FlatList, ScrollView, TouchableOpacity
} from 'react-native'

import styles from './styles'

let arrayholder = [];
let newData;

export default class InActiveCrewLeader extends React.Component {


    state = {
        search: '',
    };
    constructor({ navigation }) {
        super({ navigation });
        this.state = {

            text: '',
            data: [
                { customer_name: 'Riya', job_name: 'Gardening', },
                { customer_name: 'Akash', job_name: 'Cleaning', },
                { customer_name: 'kirti', job_name: 'HouseKeeping', },
                { customer_name: 'Neah', job_name: 'Gardening', },
                { customer_name: 'Radheshyam', job_name: 'Cleaning', },
                { customer_name: 'Shiwa', job_name: 'HouseKeeping', },
            ],
            search: false,
            resultData: [],
        }
    }
    searchData(text) {
        arrayholder = [...this.state.data];
        if (text.length > 0) {
            this.setState({ search: true })
        } else {
            this.setState({ search: false })
        }

        let textData = text.toUpperCase();
        newData = arrayholder.filter(item => item.job_name.toUpperCase().includes(textData))
        console.log("result==>", newData)
    }



   

    OnbackClick = (props) => {
        this.props.navigation.goBack()
        console.warn(props.navigation)
    }

    render() {
        const { search } = this.state;
        return (
            <View style={{ flex: 1 }}>
               <View style={styles.CradContainer}>
                    <TouchableOpacity onPress={this.OnbackClick} style={styles.BackContainer}>
                        <Image
                            source={require('../../../images/back.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={styles.NotificationContainer}>
                            <Text style={{ fontSize: 16, fontStyle: 'italic', fontWeight: 'normal', color: '#3AB34A', marginBottom: 8 }} >06</Text>
                        </View>
                        <Text style={styles.DateContainer}>Inactive crew lead</Text>

                    </View>
                </View>
                <View style={{ flex: 5, }}>
                    <ScrollView>
                        <View style={styles.SearchContainer}>
                            <TouchableOpacity>
                                <Image style={{ width: 30, height: 30, margin: 12 }} source={require('../../../images/search.png')} />
                            </TouchableOpacity>
                            <TextInput
                                placeholder="Search by name"
                                onChangeText={(text) => this.searchData(text)}
                                keyboardType='default'
                                blurOnSubmit={false}
                            />
                        </View>


                        <FlatList data={this.state.search ? newData : this.state.data}
                            renderItem={({ item }) =>
                                <View onStartShouldSetResponder={()=>this.props.navigation.navigate('CrewLeaderDetails')} style={styles.JobItemContainer}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 50, height: 50, marginTop: 15, marginLeft: "10%", flexWrap: 'wrap' }} source={require('../../../images/backgroundImage.png')} />

                                        <View style={{marginLeft:"10%"}}>

                                        <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 15,  fontStyle: 'italic', color: '#000' }}> {item.customer_name} </Text>
                                        <Text style={{ fontWeight: 'bold', fontSize: 14,   marginTop: "4%", fontStyle: 'italic', color: '#3AB34A' }}>Crew lead </Text>

                                        </View>

                                        <Image style={{ marginTop: 10,position:'absolute',right: "10%",bottom:"1%"}} source={require('../../../images/next_arrow.png')}></Image>

                                    </View>
                               
                                  
                                   
                                </View>



                            }
                            keyExtractor={(_, index) => index.toString()}
                        />

                    </ScrollView>
                </View>






            </View>
        )
    }
}





