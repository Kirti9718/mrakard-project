import React from 'react';
import {
  View, Text, TouchableOpacity,
  createDrawerNavigator, TextInput, Button, Image, StyleSheet, SafeAreaView
} from 'react-native'

import  styles from './styles'

class CrewManagement extends React.Component {
  constructor(props) {
    super(props)

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.CradContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Image style={styles.MenuContainer}
              source={require('../../images/menu.png')} />
          </TouchableOpacity>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Image style={styles.MkradContainer}
              source={require('../../images/mkrad_removbg.jpg')} />
            <View style={styles.RectangleContainer}>
              <Text style={{ marginTop: "5%", fontSize: 18, fontWeight: 'bold', color: '#fff' }}>Crew management</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('AdminNotification')}>
            <Image style={styles.NotificationConrainer}
              source={require('../../images/notification.png')} />
          </TouchableOpacity>
        </View>

        <View style={{ flex: 5, flexDirection: 'column' }}>
        
          <View onStartShouldSetResponder={() => this.props.navigation.navigate("ActiveCrewLeader")} style={styles.JobItemContainer}>
            <View style={styles.NotificationContainer}>
              <Text style={{ fontSize: 14, fontStyle: 'italic', fontWeight: 'bold', color: '#3AB34A', }} >06</Text>
            </View>
            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "12%", fontStyle: 'italic', color: '#898989' }}>Active crew lead</Text>
            <Image style={{ marginLeft: "18%" }} source={require('../../images/next_arrow.png')}></Image>
          </View>
          <View onStartShouldSetResponder={() => this.props.navigation.navigate("InActiveCrewLeader")} style={styles.JobItemContainer_2}>
            <View style={styles.NotificationContainer}>
              <Text style={{ fontSize: 14, fontStyle: 'italic', fontWeight: 'bold', color: '#3AB34A', }} >01</Text>
            </View>
            <Text style={{ fontWeight: 'bold', fontSize: 14, marginLeft: "10%", fontStyle: 'italic', color: '#898989' }}>Inactive crew lead</Text>
            <Image style={{ marginLeft: "18%" }} source={require('../../images/next_arrow.png')}></Image>
          </View>


        
        </View>
        <View style={{
          flex: .7, backgroundColor: '#222441', marginTop: "20%"
          , flexDirection: 'row', alignItems: 'center'
        }}>


          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("AdminHome")} style={{ alignItems: 'center', alignSelf: 'center', }} >
              <Image style={{ width: 20, height: 20, }} source={require('../../images/home.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("SchedularScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 25, height: 25, }} source={require('../../images/scheduler.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("CrewManagement")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 20, height: 20, }} source={require('../../images/teamwork.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.Bottombar}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("AdminProfileScreen")} style={{ alignItems: 'center', alignSelf: 'center' }}>
              <Image style={{ width: 20, height: 20, }} source={require('../../images/profile.png')} />
            </TouchableOpacity>
          </View>
        </View>

      </View>
    )
  }
}



export default CrewManagement


