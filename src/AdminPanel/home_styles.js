import { StyleSheet } from 'react-native'


const styles = StyleSheet.create({



    CradContainer: {
        flex: 1.5,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 15,
        shadowRadius: 30,
        borderWidth: 0,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,

    },
    BackContainer: {
        width: "20%",
        height: "10%",
        marginLeft: "5%",
        marginTop: "17%",

    },
      DateContainer: {
        marginTop: "24%",
        fontSize:18,
        fontWeight:'bold',
        color:'#898989',
        fontStyle:'italic',marginLeft:"5%"
       
    },

    NotificationContainer: {

        width: "12%",
        height: "15%",
        marginTop: "4%",
        alignSelf: 'center',
        borderRadius: 25,
        borderColor: '#3AB34A',
        borderWidth: 1,
        alignItems: 'center',
    },
    reverseBtn:{
     width:40,
     height:40,
     borderRadius:40/2, 
     alignSelf: 'center',
    
    },

    cancelJob: {
        fontSize: 18,
        fontWeight: "bold",
        color: '#FF2F2F',
        marginTop: "5%",
        alignSelf:'center',
        fontStyle:'italic'


    },


    forwordBtn:{
        width:40,
        height:40,
        borderRadius:40/2,
        alignSelf: 'center',
      

       },
   
    RectangleContainer: {
        width: 200,
        height: 50,
        marginTop: "11%",
        backgroundColor: '#3AB34A',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        alignItems: 'center',
        marginLeft: "5%"
    },

    NotificationConrainer: {
        marginTop: "95%",
        marginRight: "8%"
    },
   

    JobItemContainer: {

        width: 350,
        height: 200,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#fff',
        shadowColor: '#fff',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 3,
        marginBottom: 20

    },

    CancelJobContainer: {
        marginTop: "6%",
        width: 350,
        height: 200,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#fff',
        shadowColor: '#fff',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 3,
        marginBottom: 20

    },

    createjobschedule:{
        width: 350,
        height: 260,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
         marginTop: "6%",
      
       
    },
    TextJobCreate:{
        fontStyle:'italic',
        fontWeight:'bold',
        color:'#3AB34A',
        fontSize:18,
        marginLeft:"16%",
        textDecorationLine:'underline'
    },
    separator: {
        marginTop: 12,
        borderLeftWidth: 1,
        borderLeftColor: '#000',
      
    },
    TextContainer_13: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: "10%",
        fontStyle: 'italic',
        color: '#000',
        alignSelf: 'center'

    },

    TextContainer_2: {
        fontSize: 18,
        fontWeight: "bold",
        color: '#fff',
        marginTop: "5%"


    },

    SaveButton:{
        backgroundColor: '#3AB34A',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#222441',
        height: 50,
        width: 200,
        alignItems: 'center',
        borderRadius: 30,
        alignSelf: 'center',
        marginTop:"5%"

    },
    JonMarked_Completed_Modal: {

        width: 350,
        height: 310,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 14,
        borderColor: '#ddd',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 5,
        shadowRadius: 10,
        marginBottom: 10,
        position: 'absolute',
        bottom: "-1%",
        


    },

    RectangleContainer_3: {
        width: "70%",
        
        marginTop: "5%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%",   
       
    },

    
    RectangleContainer_5: {
        width: "70%",
        marginTop: "5%",
        borderColor: '#3AB34A',
        borderRadius: 24,
        borderWidth:2,
        alignItems: 'center',
        marginLeft: "15%", 
        flexDirection:'row'  ,
        height:45

       
    },

    PickerContainer:{height: 35, 
        width: "90%",
        marginLeft:"5%",
      },

    tileContainer: {
        flex:1,
        height: 80,
        marginTop: "5%",
        backgroundColor: '#3AB34A',
        flexDirection: 'row',
        alignItems:'center'

    },
    tileText:{
        fontSize:20,
        fontWeight:'bold',
        color:'#fff',
        fontStyle:'italic',
        alignSelf:'center',
      
      
    }

})
export default styles