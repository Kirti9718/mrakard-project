import React from 'react';
import { Picker, View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import styles from './ongoingjob_styles'
import DateTimePickerModal from "react-native-modal-datetime-picker";



let oneTimeSelected_time
let oneTimeSelected_date
let monthNames = ["Jan", "Feb", "March", "April", "May", "June",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"
];
class AdminOnGoingJob extends React.Component {

    constructor() {
        super();
        this.state = {
            data: [
                { Job_request: 'lawn maintenance', Job_Location: '142 Victoria Court, Fort Kent, ME, Maine-04743', time_slot: '09:30 AM to 10:30 AM', customer_name: 'Jerry Paul' },
            ],
            isModalVisible: false,
            isModalVisible2:false,
            isTimePickerVisible: false,
            oneTimeSelected_time: "",
            isDatePickerVisible: false,
            oneTimeSelected_date: ""
         
        }
    }

    Separator = () => (
        <View style={styles.separator} />
    );
    OnbackClick = (props) => {
        this.props.navigation.goBack()
      
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
        
    };

    toggleModal2 = () => {
        this.setState({ isModalVisible2: !this.state.isModalVisible2 });
       
    };

    OpenOnCancelScreeen=()=>{
        // this.setState({ isModalVisible: !this.state.isModalVisible });
         this.props.navigation.navigate('AdminCancelJob')
         this.setState({ isModalVisible: !this.state.isModalVisible });
        
         
        }


        showDatePicker = () => {
            this.setState({ isDatePickerVisible: true });
        };
        hideDatePicker = () => {
            this.setState({ isDatePickerVisible: false })
        }
        showTimePicker = () => {
            this.setState({ isTimePickerVisible: true });
        };
    
        hideTimePicker = () => {
            this.setState({ isTimePickerVisible: false })
        }
    
        handleTimePicked = (time) => {
            let AM_PM;
            if (time.getHours() < 12) {
                AM_PM = "AM";
            } else {
                AM_PM = "PM";
            }
            oneTimeSelected_time = time.getHours() + ':' + time.getMinutes() + " " + AM_PM;
            this.setState({ oneTimeSelected_time })
            this.hideTimePicker();
    
        };
        handleDatePicked = (date) => {
            oneTimeSelected_date = date.getDate() + " " + monthNames[(date.getMonth())] + " ," + date.getFullYear();
            this.setState({ oneTimeSelected_date })
            this.hideDatePicker();
        };
    render() {
        return (


            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity
                        onPress={this.OnbackClick} style={styles.BackContainer} >
                        <Image source={require('../images/back.png')} />
                    </TouchableOpacity>
                    <View style={styles.NotificationContainer}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'normal', color: '#3AB34A', marginBottom: 8 }} >06</Text>
                    </View>
                    <View style={{ alignSelf: 'center', marginTop: 18, marginLeft: 15 }}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'bold', color: '#898989' }}>Ongoing job</Text>
                    </View>
                </View>

              
                    <View style={{ flex: 4 }}>


                        <FlatList data={this.state.data}
                            renderItem={({ item }) =>

                                <View style={styles.JobItemContainer}>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={styles.ImageContainer} source={require('../images/backgroundImage.png')} />
                                        <Text style={styles.TextContainer_4}> {item.customer_name} </Text>

                                    </View>
                                    <this.Separator />
                                    <Text style={styles.TextContainer_5}> Job Request</Text>
                                    <Text style={styles.TextContainer_6}> {item.Job_request}</Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('GetDirectionScreen')} style={{ position: 'absolute', right: "10%", top: "39%", }}>
                                        <Text style={styles.TextContainer_7}> Get direction</Text>
                                        <Image style={styles.TextContainer_8} source={require('../images/directions_button.png')} />
                                    </TouchableOpacity>
                                    <this.Separator />

                                    <Text style={styles.TextContainer_5}> Time slot</Text>
                                    <Text style={styles.TextContainer_6}> {item.time_slot}</Text>
                                    <TouchableOpacity onPress={() => alert('hijhh')} style={styles.TextContainer_9}>
                                    
                                    </TouchableOpacity>
                                    <this.Separator />
                                    <Text style={styles.TextContainer_11}>Job Location </Text>
                                    <Text style={styles.TextContainer_12}> {item.Job_Location}</Text>
                                    <Image style={styles.LocationContainer} source={require('../images/location.png')} />

                                </View>
                            }
                        />
                    </View>
                    <View style={{flexDirection:'row',flex:3,marginTop:"0%",marginLeft:"8%"}}>
                                 <TouchableOpacity activeOpacity={.5} onPress={this.toggleModal}><Text style={styles.TextJobCreate}>Reschedule</Text></TouchableOpacity>
                                 {this.Separator}
                                 <TouchableOpacity activeOpacity={.5} onPress={this.toggleModal2} ><Text style={styles.TextJobCreate}>Change crew lead</Text></TouchableOpacity>

                             </View>

                             
                             <Modal isVisible={this.state.isModalVisible} backdropOpacity={0.1}>
                            <View style={styles.JonMarked_Completed_Modal}>
                                <Text style={styles.TextContainer_13}>Reschedule job</Text>

                                          <TouchableOpacity style={styles.RectangleContainer_5} onPress={this.showDatePicker} >
                                                <TextInput editable={false} placeholder="select date" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_date} ></TextInput>
                                                <DateTimePickerModal
                                                    isVisible={this.state.isDatePickerVisible}
                                                    mode="date"
                                                    onConfirm={this.handleDatePicked}
                                                    onCancel={this.hideDatePicker}
                                                    forment="dd-MM-y"


                                                />
                                                <Image style={{ alignSelf: 'center', marginLeft: "35%", width: 20, height: 20 }} source={require('../images/date_picker.png')} />

                                            </TouchableOpacity>

                                            <TouchableOpacity style={styles.RectangleContainer_5} onPress={this.showTimePicker} >
                                                <TextInput editable={false} placeholder="select time" style={{ alignSelf: 'center', marginLeft: "13%", color: '#000', fontSize: 16 }} value={this.state.oneTimeSelected_time} ></TextInput>

                                                <DateTimePickerModal
                                                    isVisible={this.state.isTimePickerVisible}
                                                    mode="time"
                                                    onConfirm={this.handleTimePicked}
                                                    onCancel={this.hideTimePicker}
                                                    forment="dd-MM-y"
                                                    amPmAriaLabel="Select AM/PM"
                                                    is24Hour={false}


                                                />
                                                <Image style={{ marginLeft: "35%", width: 30, height: 30 }} source={require('../images/time.png')} />

                                            </TouchableOpacity>
                                         
                                <TouchableOpacity activeOpacity={.5} style={styles.SaveButton} title="Hide modal" onPress={this.toggleModal}  >
                                    <Text style={styles.TextContainer_2}>Save</Text>
                                </TouchableOpacity>

                                <TouchableOpacity activeOpacity={.5}  title="Hide modal" onPress={this.OpenOnCancelScreeen}  >
                                    <Text style={styles.cancelJob}>Cancel job</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal> 
                      
                        <Modal isVisible={this.state.isModalVisible2} backdropOpacity={0.2}>
                            <View style={styles.JonMarked_Completed_Modal}>
                                <Text style={styles.TextContainer_13}>Change crew lead</Text>

                                <TouchableOpacity style={styles.RectangleContainer_3}  >
                                        <Picker style={styles.PickerContainer}
                                            selectedValue={this.state.language4}
                                            onValueChange={(itemValue4, itemPosition4) =>
                                                this.setState({ language4: itemValue4, choosenIndex: itemPosition4 })}
                                        >
                                            <Picker.Item label="John smith" value="java" />
                                            <Picker.Item label="Kitty" value="js" />
                                            <Picker.Item label="Robin" value="rn" />

                                        </Picker>
                                    </TouchableOpacity>
                                <TouchableOpacity activeOpacity={.5} style={styles.SaveButton} title="Hide modal" onPress={this.toggleModal2}  >
                                    <Text style={styles.TextContainer_2}>Reassign</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal> 
                      

            </View>

        )
    }
}




export default AdminOnGoingJob;