import React from 'react';
import { Button, View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';

import styles from './Jobdetails_styles'

class AdminJobDetails extends React.Component {

    constructor() {
        super();
        this.state = {
            data: [
                { Job_request: 'lawn maintenance', Job_Location: '142 Victoria Court, Fort Kent, ME, Maine-04743', time_slot: '09:30 AM to 10:30 AM', customer_name: 'Jerry Paul' },
            ],
         
        }
    }

    Separator = () => (
        <View style={styles.separator} />
    );
    OnbackClick = (props) => {
        this.props.navigation.goBack()
      
    }

    render() {
        return (


            <View style={{ flex: 1 }}>
                <View style={styles.CradContainer}>
                    <TouchableOpacity
                        onPress={this.OnbackClick} style={styles.BackContainer} >
                        <Image source={require('../images/back.png')} />
                    </TouchableOpacity>
                   
                    <View style={{ alignSelf: 'center', marginTop: 18, marginLeft: 15 }}>
                        <Text style={{ fontSize: 18, fontStyle: 'italic', fontWeight: 'bold', color: '#898989' }}>Job detail</Text>
                    </View>
                </View>

              
                    <View style={{ flex: 4 }}>


                        <FlatList data={this.state.data}
                            renderItem={({ item }) =>

                                <View style={styles.JobItemContainer}>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={styles.ImageContainer} source={require('../images/backgroundImage.png')} />
                                        <Text style={styles.TextContainer_4}> {item.customer_name} </Text>

                                    </View>

                                    <View style={{ flexDirection: 'row',position:'absolute',right:"34%" ,top:"20%"}}>
                                        <Text style={styles.CostContainer}>Job Cost:</Text>
                                        <Text style={styles.TextContainer_6}> $20.00</Text>

                                    </View>
                                  
                                    <this.Separator />
                                    <Text style={styles.TextContainer_5}> Job Request</Text>
                                    <Text style={styles.TextContainer_6}> {item.Job_request}</Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('GetDirectionScreen')} style={{ position: 'absolute', right: "10%", top: "39%", }}>
                                        <Text style={styles.TextContainer_7}> Get direction</Text>
                                        <Image style={styles.TextContainer_8} source={require('../images/directions_button.png')} />
                                    </TouchableOpacity>
                                    <this.Separator />

                                    <Text style={styles.TextContainer_5}> Time slot</Text>
                                    <Text style={styles.TextContainer_6}> {item.time_slot}</Text>
                               
                                    <this.Separator />
                                    <Text style={styles.TextContainer_11}>Job Location </Text>
                                    <Text style={styles.TextContainer_12}> {item.Job_Location}</Text>
                                    <Image style={styles.LocationContainer} source={require('../images/location.png')} />

                                </View>
                            }
                        />
                    </View>

                    
          


            </View>

        )
    }
}




export default AdminJobDetails;