// In App.js in a new project

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './../screen/Home/index'
import ProfileScreen from './../screen/Profile/index'
import LoginScreen from '../screen/Login';
import  LoginPage from '../screen/Login/LoginDetails';
import  DashboardScreen from './index'
import  SignUpPage from '../screen/Signup/Signup_index'
import  DashboardNotification  from '../Dashboard/DashboardNotification'
import  TermCondition from '../screen/TermCondition/Termcondition'
import AboutUs from '../screen/Aboutus/AboutUs';
import Disclaimer from '../screen/Disclaimer/Disclaimer'
import ServiceReportScreen from '../screen/ServiceReport/ServiceReport'
import CalenderScreen from '../screen/Calender/Calender'
import NewJobAssign from '../screen/NewJobAssigned/NewJob';
import GetDirectionScreen from '../screen/NewJobAssigned/GetDirection';
import OnGoingJobScreen from '../screen/OnGoingJob/OnGoingJob';
import CameraScreen from '../screen/OnGoingJob/MyCamera/Camera';
import OnPauseScreen from '../screen/OnPauseJob/OnPauseJob';
import OnCompleteJobScreen from '../screen/ServiceReport/CompleteJob';
import AssignJobListOnDate from '../screen/Calender/AssignJobListOnDate';
import ChangePassword from '../screen/Profile/ChangePassword';
import EditProfile from '../screen/Profile/EditDetails';
import AdminLoginPage from '../AdminPanel/AdminLogin';
import AdminHome from '../AdminPanel/AdminHome';
import AdminJobScheduleList from '../AdminPanel/AdminJobScheduleList';
import AdminCancelJob from '../AdminPanel/AdminCancelJob';
import AdminJobDetails from '../AdminPanel/AdminJobDetails';
import AdminOnGoingJob from '../AdminPanel/AdminOngoingJob';
import SchedularScreen from '../AdminPanel/Schedular/Schedular';
import CrewManagement from '../AdminPanel/Crew_Management/Crew_management';
import ActiveCrewLeader from '../AdminPanel/Crew_Management/ActiveCrewLeader/ActiveCrewLeader';
import CreateNewJob from '../AdminPanel/Schedular/CreateNewJobSchedular/CreateNewJob';
import InActiveCrewLeader from '../AdminPanel/Crew_Management/InActiveCrewLeader/InActiveCrewLeader';
import CrewLeaderDetails from '../AdminPanel/Crew_Management/CrewLeader_details/Crewleaderdetail';
import AdminProfileScreen from '../AdminPanel/AdminProfile/Profile';
import AdminEditProfile from '../AdminPanel/AdminProfile/Profile/EditDetails';
import AdminChangePassword from '../AdminPanel/AdminProfile/Profile/ChangePassword';
import AdminDrawerNavs from './admin_index';
import NewCreateJob from '../AdminPanel/Schedular/CreateNewJobSchedular/NewJobCreate';
import OnUpComingJobScreen from '../screen/Calender/Upcoming _job';
import AdminAboutUs from '../AdminPanel/AdminDrawarScreen/Aboutus/AboutUs';
import AdminDisclaimer from '../AdminPanel/AdminDrawarScreen/Aboutus/Disclaimer/Disclaimer';
import AdminTermCondition from '../AdminPanel/AdminDrawarScreen/Aboutus/TermCondition/Termcondition';
import AdminNotification from '../AdminPanel/AdminNotification/DashboardNotification';

import TimeScreen from '../TimeSelection'

const Stack = createStackNavigator();

function AppNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode = "none">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="LoginPage" component={LoginPage}/>
        <Stack.Screen name="DashboardScreen" component={DashboardScreen} />
        <Stack.Screen name="SignupScreen" component={SignUpPage} />
        <Stack.Screen name="DashboardNotification" component={DashboardNotification} />
        <Stack.Screen name="TermCondition" component={TermCondition} />
        <Stack.Screen name="AboutUs" component={AboutUs} />
        <Stack.Screen name="Disclaimer" component={Disclaimer} />
        <Stack.Screen name="ServiceReportScreen" component={ServiceReportScreen} />
        <Stack.Screen name="CalenderScreen" component={CalenderScreen} />
        <Stack.Screen name="NewJobAssignScreen" component={NewJobAssign} />
        <Stack.Screen name="GetDirectionScreen" component={GetDirectionScreen} />
        <Stack.Screen name="OnGoingJobScreen" component={OnGoingJobScreen} />
        <Stack.Screen name="CameraScreen" component={CameraScreen} />
        <Stack.Screen name="OnPauseScreen" component={OnPauseScreen} />
        <Stack.Screen name="OnCompleteJobScreen" component={OnCompleteJobScreen} />
        <Stack.Screen name="AssignJobListOnDate" component={AssignJobListOnDate} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />
        <Stack.Screen name="EditProfile" component={EditProfile} />
        <Stack.Screen name="AdminLoginPage" component={AdminLoginPage} />
        <Stack.Screen name="AdminHome" component={AdminHome} />
        <Stack.Screen name="AdminJobScheduleList" component={AdminJobScheduleList} />
        <Stack.Screen name="AdminCancelJob" component={AdminCancelJob} />
        <Stack.Screen name="AdminJobDetails" component={AdminJobDetails} />
        <Stack.Screen name="AdminOnGoingJob" component={AdminOnGoingJob} />
        <Stack.Screen name="SchedularScreen" component={SchedularScreen} />
        <Stack.Screen name="CrewManagement" component={CrewManagement} />
        <Stack.Screen name="ActiveCrewLeader" component={ActiveCrewLeader} />
        <Stack.Screen name="CreateNewJob" component={CreateNewJob} />
        <Stack.Screen name="InActiveCrewLeader" component={InActiveCrewLeader} />
        <Stack.Screen name="CrewLeaderDetails" component={CrewLeaderDetails} />
        <Stack.Screen name="AdminProfileScreen" component={AdminProfileScreen} />
        <Stack.Screen name="AdminEditProfile" component={AdminEditProfile} />
        <Stack.Screen name="AdminChangePassword" component={AdminChangePassword} />
        <Stack.Screen name="AdminDrawerNavs" component={AdminDrawerNavs} />
        <Stack.Screen name="NewCreateJob" component={NewCreateJob} />
        <Stack.Screen name="OnUpComingJobScreen" component={OnUpComingJobScreen} />
        <Stack.Screen name="AdminAboutUs" component={AdminAboutUs} />
        <Stack.Screen name="AdminDisclaimer" component={AdminDisclaimer} />
        <Stack.Screen name="AdminTermCondition" component={AdminTermCondition} />
        <Stack.Screen name="AdminNotification" component={AdminNotification} />
        <Stack.Screen name="TimeScreen" component={TimeScreen} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default AppNavigation;